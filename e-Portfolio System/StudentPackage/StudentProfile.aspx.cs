﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.StudentPackage
{
    public partial class UpdateStudentProfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            displayprjgv();



        }

        protected void btnStudentProfileUpdate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Response.Redirect("StudentProfileUpdate.aspx");
            }
        }

        protected void displayprjgv()
        {
            // Create a new Student object
            Student objStudent = new Student();

            // Read Student ID from query string 
            objStudent.studentId = (int)Session["StudentID"];
     
            // Load student information to controls
            int errorCode = objStudent.getPortfolioDetails();
            if (errorCode == 0)
            {
                imgStudentProfileImage.ImageUrl = "~/Images/" + objStudent.photo;
                lblStudentProfileDesc.Text = objStudent.description;
                lblStudentProfileAchievement.Text = objStudent.achievement;
                lblStudentProfileLinks.Text = objStudent.externalLink;
                lblStudentProfileEmailAddr.Text = objStudent.emailAddr;

                if (objStudent.studentSkillList.Count() > 0)
                {
                    lblStudentProfileSkillset.Text = objStudent.studentSkillList[0].ToString();
                    if (objStudent.studentSkillList.Count() > 1)
                    {
                        for (int i = 1; i < objStudent.studentSkillList.Count; i++)
                            lblStudentProfileSkillset.Text += "; " + objStudent.studentSkillList[i].ToString();
                    }
                }
            }
        }
    }
}