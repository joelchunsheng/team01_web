﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentPackage/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="StudentProfile.aspx.cs" Inherits="e_Portfolio_System.StudentPackage.UpdateStudentProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style5 {
            width: 157px;
        }
        .auto-style6 {
            width: 157px;
            height: 26px;
        }
        .auto-style7 {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        Your Profile</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style5"><strong>Profile Image:</strong></td>
            <td>
                <asp:Image ID="imgStudentProfileImage" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="auto-style6"><strong>Skillsets:</strong></td>
            <td class="auto-style7">
                <asp:Label ID="lblStudentProfileSkillset" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Self-Description:</strong></td>
            <td>
                <asp:Label ID="lblStudentProfileDesc" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Acheivements:</strong></td>
            <td>
                <asp:Label ID="lblStudentProfileAchievement" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Links:</strong></td>
            <td>
                <asp:Label ID="lblStudentProfileLinks" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Email Address:</strong></td>
            <td>
                <asp:Label ID="lblStudentProfileEmailAddr" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:Button ID="btnStudentProfileUpdate" runat="server" OnClick="btnStudentProfileUpdate_Click" Text="Update" />
</asp:Content>
