﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentPackage/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="StudentProjectUpdate.aspx.cs" Inherits="e_Portfolio_System.StudentPackage.StudentProjectUpdate" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style6 {
        width: 100%;
        height: 215px;
    }
    .auto-style18 {
        width: 65px;
        height: 56px;
    }
    .auto-style19 {
        height: 56px;
        width: 225px;
    }
    .auto-style20 {
        height: 56px;
    }
    .auto-style21 {
        width: 65px;
        height: 57px;
    }
    .auto-style22 {
        height: 57px;
        width: 225px;
    }
    .auto-style23 {
        height: 57px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        Update Project</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <table class="auto-style6">
    <tr>
        <td class="auto-style18"><strong>Project Title:</strong></td>
        <td class="auto-style19">
            <asp:TextBox ID="txtStdUptTitle" runat="server" Width="215px"></asp:TextBox>
        </td>
        <td class="auto-style20">
            <asp:RequiredFieldValidator ID="rfvUptPrjTitle" runat="server" ControlToValidate="txtStdUptTitle" Display="Dynamic" ErrorMessage="Please enter a title"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style18"><strong>Poster Image:</strong></td>
        <td class="auto-style19">
            <asp:Image ID="imgPhoto" runat="server" Height="100px" />
            <asp:FileUpload ID="UPphoto" runat="server" />
        </td>
        <td class="auto-style20">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style21"><strong>Team Members:</strong></td>
        <td class="auto-style22">
            <asp:TextBox ID="txtStdUptTeam" runat="server" Width="215px"></asp:TextBox>
        </td>
        <td class="auto-style23">
            <asp:Button ID="btnStdUptAdd" runat="server" Text="+" />
        </td>
    </tr>
    <tr>
        <td class="auto-style21"><strong>Project
            <br />
            Description:</strong></td>
        <td class="auto-style22">
            <asp:TextBox ID="txtStdUptDesc" runat="server" Height="75px" Width="215px"></asp:TextBox>
        </td>
        <td class="auto-style23">
            <asp:RequiredFieldValidator ID="rfvUptPrjDesc" runat="server" ControlToValidate="txtStdUptDesc" Display="Dynamic" ErrorMessage="Please enter your project description"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style21"><strong>Project Link:</strong></td>
        <td class="auto-style22">
            <asp:TextBox ID="txtStdUptLink" runat="server" Width="215px"></asp:TextBox>
        </td>
        <td class="auto-style23">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style21"><strong>Project Reflection</strong></td>
        <td class="auto-style22">
            <asp:TextBox ID="txtStdUptReflection" runat="server" Width="317px" Height="47px"></asp:TextBox>
        </td>
        <td class="auto-style23">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtStdUptReflection" Display="Dynamic" ErrorMessage="Please enter your project reflection"></asp:RequiredFieldValidator>
        </td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <br />
<asp:Button ID="btnStdUptUpdate" runat="server" Text="Update" OnClick="btnStdUptUpdate_Click" />
<asp:Button ID="btnStdUptCreate" runat="server" Text="Create" OnClick="btnStdUptCreate_Click" />
</asp:Content>
