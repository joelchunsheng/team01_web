﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
namespace e_Portfolio_System.StudentPackage
{
    public partial class StudentProjectCreate2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
       
        }

   

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            //create a new object from the staff class
            Project objProject = new Project();


            string savePath;
            string fileExt = Path.GetExtension(UPphoto.FileName);
            savePath = MapPath("~/Images/Projects/" + fileExt);
            UPphoto.SaveAs(savePath);
            imgPhoto.ImageUrl = "~/Images/" + fileExt;

            objProject.title = txtStdCrtTitle.Text;
            objProject.projectPoster = savePath;
            objProject.description = txtStdCrtDesc.Text;
            objProject.projectUrl = txtStdCrtLink.Text;


            int SkillsID = cclTeam.SelectedIndex + 1;
            List<string> TeamMembers = new List<string>();

            if (cclTeam.SelectedIndex >= 0)
            {
                foreach (ListItem item in cclTeam.Items)
                {
                    int NoOfItems = cclTeam.Items.IndexOf(item) + 1;
                    if (item.Selected && !TeamMembers.Contains(NoOfItems.ToString()))
                    {
                        TeamMembers.Add(NoOfItems.ToString());
                    }

                }
                for (int i = 0; i < TeamMembers.Count; i++)
                {
                    objProject.addProjectMember(objProject.studentId.ToString(), TeamMembers[i]);
                }
            }


            //call the add method to insert the staff record to database
            int errorCode = objProject.addProjectStudent();


            if (errorCode == -2)
            {
                lblMessage.Text = "Unable to create project please try again";
            }

            else
            {
                lblMessage.Text = "Student project created successfully";

            }
            Response.Redirect("StudentProjectView.aspx");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentProjectView.aspx");
        }

        protected void btnStdCrtAdd_Click(object sender, EventArgs e)
        {
            Project objProject = new Project();


            objProject.studentId = (int)Session["StudentID"];
            string chkstudentId = (string)Session["StudentID"];
            string chkprojectId = Convert.ToString(objProject.projectId);


            int errorCode = objProject.addProjectMember(chkstudentId, chkprojectId);


            if (errorCode == 0)
            {
                lblMessage.Text = "Staff record has been updated successfully." ;
                Response.Redirect("StudentMain.aspx");
            }
            else if (errorCode == -2)
            {
                lblMessage.Text = "Unable to save record as staff is not found." + objProject.studentId;
                lblMessage.ForeColor = System.Drawing.Color.Red;

                Response.Redirect("StudentMain.aspx");
            }
        }
    }
}