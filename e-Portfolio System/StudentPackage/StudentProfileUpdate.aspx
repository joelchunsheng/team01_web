﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentPackage/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="StudentProfileUpdate.aspx.cs" Inherits="e_Portfolio_System.StudentPackage.StudentProfileUpdate1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style5 {
            width: 135px;
        }
        .auto-style6 {
            width: 270px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        Update Profile</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style5"><strong>Profile Image:</strong></td>
            <td class="auto-style6">
                <asp:FileUpload ID="UPphoto" runat="server" />
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Skillsets:</strong></td>
            <td class="auto-style6">
                <asp:CheckBoxList ID="cclSkillset2" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="SkillSetName" DataValueField="SkillSetID">
                </asp:CheckBoxList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Student_EPortfolioConnectionString %>" SelectCommand="SELECT * FROM [SkillSet]"></asp:SqlDataSource>
            </td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Self-Description:</strong></td>
            <td class="auto-style6">
                <asp:TextBox ID="txtUptProfileDesc" runat="server" Height="75px" Width="215px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUptProfileDesc" ErrorMessage="Please enter yourself description"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Achievements:</strong></td>
            <td class="auto-style6">
                <asp:TextBox ID="txtUptProfileAchieve" runat="server" Height="75px" Width="215px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtUptProfileAchieve" ErrorMessage="Please enter your acheivements"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Links:</strong></td>
            <td class="auto-style6">
                <asp:TextBox ID="txtUptProfileLink" runat="server" Width="215px"></asp:TextBox>
            </td>
            <td>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtUptProfileLink" ErrorMessage="Please enter a link"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style5">&nbsp;</td>
            <td class="auto-style6">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <br />
    <strong>
    <asp:Button ID="btnUptProfileUpdate" runat="server" Text="Update" OnClick="btnUptProfileUpdate_Click" />
    <asp:Button ID="btnUptProfileCancel" runat="server" Text="Cancel" OnClick="btnUptProfileCancel_Click" />
    </strong>
</asp:Content>
