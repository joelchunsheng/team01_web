﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentPackage/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="StudentViewFeedback.aspx.cs" Inherits="e_Portfolio_System.StudentPackage.StudentViewFeedback" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        View
    Feedback</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:GridView ID="gvStdFeedback" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="MentorID" HeaderText="Mentor" />
            <asp:BoundField DataField="Description" HeaderText="Description" />
            <asp:BoundField DataField="DateCreated" HeaderText="Date Issued" />
            <asp:BoundField DataField="Status" HeaderText="Status" />
            <asp:HyperLinkField DataNavigateUrlFields="StudentID,Name" DataNavigateUrlFormatString="SuggestionResolve.aspx?staffid={0}&amp;name={1}" Text="Resolve" />
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    </asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Content>
