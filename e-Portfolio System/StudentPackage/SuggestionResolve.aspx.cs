﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.StudentPackage
{
    public partial class SuggestionResolve : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                Suggestion objSuggestion = new Suggestion();

                //Read Staff ID from query string
                objSuggestion.studentId = (int)Session["StudentID"];

                //Get salary of staff
                objSuggestion.status = "Y";


                int errorCode = objSuggestion.update();

                if (errorCode == 0)
                {
                    // Display successful message
                    lblMessage.Text = "Student " + Request.QueryString["studentId"] + " - " + Request.QueryString["name"] + "'s suggestion has been successfully approved.";
                    lnkReturn.Visible = true;
                }
                else
                {
                    // Display error message
                    lblMessage.Text = "suggestion request unsuccessful. Please try again.";
                    lnkReturn.Visible = true;
                }
            }
        }
    }

    