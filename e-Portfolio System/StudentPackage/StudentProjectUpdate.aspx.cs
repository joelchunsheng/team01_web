﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;

namespace e_Portfolio_System.StudentPackage
{
    public partial class StudentProjectUpdate : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                displayExisting();
            }
        }

        public void displayExisting()
        {
            Student objStudent = new Student();
            objStudent.studentId = (int)Session["StudentID"];
            Project objProject = new Project();

            this.txtStdUptTitle.Text += objProject.title;
            this.txtStdUptDesc.Text += objProject.description;
            this.txtStdUptLink.Text += objProject.projectUrl;

            objProject.getProjectDetails();



        }

        protected void btnStdUptUpdate_Click(object sender, EventArgs e)
        {
            Project objProject = new Project();


            string savePath;
            string fileExt = Path.GetExtension(UPphoto.FileName);
            savePath = MapPath("~/Images/Projects/" + fileExt);
            UPphoto.SaveAs(savePath);
            imgPhoto.ImageUrl = "~/Images/" + fileExt;

            objProject.title = txtStdUptTitle.Text;
            objProject.projectPoster = savePath;
            objProject.description = txtStdUptDesc.Text;
            objProject.projectUrl = txtStdUptLink.Text;
            objProject.reflection = txtStdUptReflection.Text;

            int errorCode = objProject.getProjectDetails();


            if (errorCode == 0)
            {
                lblMessage.Text = "Staff record has been updated successfully." ;
            }
            else if (errorCode == -2)
            {
                lblMessage.Text = "Unable to save record as staff is not found." ;
                lblMessage.ForeColor = System.Drawing.Color.Red;

                //Response.Redirect("StudentProfile.aspx");
            }
        }

        protected void btnStdUptCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentProjectView.aspx");
        }
    }
}