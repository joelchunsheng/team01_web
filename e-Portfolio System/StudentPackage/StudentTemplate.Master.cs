﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.StudentPackage
{
	public partial class Site1 : System.Web.UI.MasterPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{


        }

        protected void navProfile_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Response.Redirect("StudentProfile.aspx");
            }
        }

        protected void navFeedback_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Response.Redirect("StudentViewFeedback.aspx");
            }
        }

        protected void navCrtProject_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Response.Redirect("StudentProjectCreate.aspx");
            }
        }

        protected void navUptProject_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Response.Redirect("StudentProjectUpdate.aspx");
            }
        }

        protected void navMain_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Response.Redirect("StudentMain.aspx");
            }

        }
    }
}