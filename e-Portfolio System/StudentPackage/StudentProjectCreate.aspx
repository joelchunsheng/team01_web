﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentPackage/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="StudentProjectCreate.aspx.cs" Inherits="e_Portfolio_System.StudentPackage.StudentProjectCreate2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style6 {
        width: 100%;
        height: 215px;
    }
    .auto-style18 {
        width: 65px;
        height: 56px;
    }
    .auto-style19 {
        height: 56px;
        width: 225px;
    }
    .auto-style20 {
        height: 56px;
    }
    .auto-style21 {
        width: 65px;
        height: 57px;
    }
    .auto-style22 {
        height: 57px;
        width: 225px;
    }
    .auto-style23 {
        height: 57px;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        Create Project</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <table class="auto-style6">
    <tr>
        <td class="auto-style18"><strong>Project Title:</strong></td>
        <td class="auto-style19">
            <asp:TextBox ID="txtStdCrtTitle" runat="server" Width="215px"></asp:TextBox>
        </td>
        <td class="auto-style20">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtStdCrtTitle" ErrorMessage="Please enter a title"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style18"><strong>Poster Image:</strong></td>
        <td class="auto-style19">
            <asp:Image ID="imgPhoto" runat="server" Height="100px" />
            <asp:FileUpload ID="UPphoto" runat="server" />
        </td>
        <td class="auto-style20">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style21"><strong>Team Members:</strong></td>
        <td class="auto-style22">
            <asp:CheckBoxList ID="cclTeam" runat="server" AutoPostBack="True">
            </asp:CheckBoxList>
        </td>
        <td class="auto-style23">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style21"><strong>Project
            <br />
            Description:</strong></td>
        <td class="auto-style22">
            <asp:TextBox ID="txtStdCrtDesc" runat="server" Height="75px" Width="215px"></asp:TextBox>
        </td>
        <td class="auto-style23">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtStdCrtDesc" ErrorMessage="Please enter your description"></asp:RequiredFieldValidator>
        </td>
    </tr>
    <tr>
        <td class="auto-style21"><strong>Project Link:</strong></td>
        <td class="auto-style22">
            <asp:TextBox ID="txtStdCrtLink" runat="server" Width="215px"></asp:TextBox>
        </td>
        <td class="auto-style23">&nbsp;</td>
    </tr>
</table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <br />
    <asp:Button ID="btnCreate" runat="server" OnClick="btnCreate_Click" Text="Create" />
&nbsp;&nbsp;
    <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Cancel" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
</asp:Content>
