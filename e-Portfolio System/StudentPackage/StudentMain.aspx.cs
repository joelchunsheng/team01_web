﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.StudentPackage
{

    public partial class StudentMain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["loginId"] != null)
            {
                lblStdMainName.Text = (string)Session["Name"];
            }
            else // user not logged in 
            {
                Response.Redirect("~/CommonPages/Login.aspx");
            }

        }

        protected void ImageButton4_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("StudentProfileUpdate.aspx");
        }

        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("StudentViewFeedback.aspx");
        }

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("StudentProjectCreate.aspx");
        }

        
    }
}