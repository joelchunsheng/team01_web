﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentPackage/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="StudentProjectView.aspx.cs" Inherits="e_Portfolio_System.StudentPackage.StudentProjectView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
        View Project</p>
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <asp:GridView ID="gvProject" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="Title" HeaderText="Title" />
            <asp:BoundField DataField="Description" HeaderText="Description" />
            <asp:HyperLinkField HeaderText="Link" DataNavigateUrlFields="ProjectURL" DataNavigateUrlFormatString="{0}" Target="_blank" Text="ProjectLink" />
            <asp:ImageField DataImageUrlField="ProjectPoster" DataImageUrlFormatString="~/Images/Projects/{0}" HeaderText="Poster" ControlStyle-Width="110" ControlStyle-Height = "155"></asp:ImageField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Create" />
&nbsp;&nbsp;
    <asp:Button ID="Button2" runat="server" OnClick="Button1_Click" Text="Update" />
&nbsp;
</asp:Content>
