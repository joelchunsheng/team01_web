﻿<%@ Page Title="" Language="C#" MasterPageFile="~/StudentPackage/StudentTemplate.Master" AutoEventWireup="true" CodeBehind="StudentMain.aspx.cs" Inherits="e_Portfolio_System.StudentPackage.StudentMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style7 {
        width: 339px;
    }
    .auto-style8 {
        width: 340px;
    }
    .auto-style9 {
        width: 1358px
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p>
    Welcome,
    <asp:Label ID="lblStdMainName" runat="server"></asp:Label>
</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <table class="auto-style9">
    <tr>
        <td class="auto-style7">
            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/Assets/Student/notepad.png" OnClick="ImageButton4_Click" Width="200px" />
            <br />
            <strong>Update<br />
            exisiting profile</strong></td>
        <td class="auto-style7">
            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/Images/Assets/Student/chat.png" OnClick="ImageButton3_Click" Width="200px" />
            <br />
            <meta charset="utf-8" />
            <span style="font-size: 12pt; font-family: Arial; color: #000000; background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre; white-space: pre-wrap;"><strong>View Feedback</strong></span></td>
        <td class="auto-style8">
            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/Assets/Student/blueprint.png" OnClick="ImageButton2_Click" Width="200px" />
            <br />
            <meta charset="utf-8" />
            <span style="font-size: 12pt; font-family: Arial; color: #000000; background-color: transparent; font-weight: 400; font-style: normal; font-variant: normal; text-decoration: none; vertical-align: baseline; white-space: pre; white-space: pre-wrap;"><strong>Create Projects</strong></span></td>
    </tr>
</table>
</asp:Content>
