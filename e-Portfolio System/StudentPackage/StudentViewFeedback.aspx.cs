﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.StudentPackage
{
    public partial class StudentViewFeedback : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            disaplayFeedbackList();
        }


        private void disaplayFeedbackList()
        {
            // Create a Mentor object.
            Suggestion objSuggestion = new Suggestion();

            // Create a DataSet object to contain the mentee list of a mentor
            DataSet result = new DataSet();

            // Call the getMentorStudent method of Mentor class to retrieve the
            // mentee details of a mentor from database.
            objSuggestion.studentId = (int)Session["StudentID"];

            int errorCode = objSuggestion.getsuggestion(ref result);
            gvStdFeedback.DataSource = result.Tables["SuggestionDetails"];
            gvStdFeedback.DataBind();

            if (errorCode == 0)
            {
                // Load Mentee information to the GridView: gvMentee
                gvStdFeedback.DataSource = result.Tables["SuggestionDetails"];
                gvStdFeedback.DataBind();

                // Display total number of mentee records
                if (result.Tables["SuggestionDetails"].Rows.Count > 0)
                    lblMessage.Text = result.Tables["SuggestionDetails"].Rows.Count + " projects record(s) found.";
                else
                    lblMessage.Text = "No projects record found.";
            }
            else
            {
                lblMessage.Text = "Smth went wrong and errorcode is not == 0 " + Convert.ToString(errorCode);
            }
        }


    }
}