﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.StudentPackage
{
    public partial class StudentProfileUpdate1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnUptProfileUpdate_Click(object sender, EventArgs e)
        {
            Student objStudent = new Student();

            //Read Staff ID from query string
            objStudent.studentId = (int)Session["StudentID"];
           
            //Get salary of staff
            objStudent.description = txtUptProfileDesc.Text;
            objStudent.achievement = txtUptProfileAchieve.Text;
            objStudent.externalLink = txtUptProfileLink.Text;


            int SkillsID = cclSkillset2.SelectedIndex + 1;
            List<string> SkillsSelected = new List<string>();

            if (cclSkillset2.SelectedIndex >= 0)
            {
                foreach (ListItem item in cclSkillset2.Items)
                {
                    int NoOfItems = cclSkillset2.Items.IndexOf(item) + 1;
                    if (item.Selected && !SkillsSelected.Contains(NoOfItems.ToString()))
                    {
                        SkillsSelected.Add(NoOfItems.ToString());
                    }

                }
                for (int i = 0; i < SkillsSelected.Count; i++)
                {
                    objStudent.SkillSetUpdate(objStudent.studentId.ToString(), SkillsSelected[i]);
                }
            }

            int errorCode = objStudent.updatePortfolio();


            if (errorCode == 0)
            {
                lblMessage.Text = "Staff record has been updated successfully."+ cclSkillset2.SelectedIndex ;
            }
            else if (errorCode == -2)
            {
                lblMessage.Text = "Unable to save record as staff is not found." + objStudent.studentId;
                lblMessage.ForeColor = System.Drawing.Color.Red;

                //Response.Redirect("StudentProfile.aspx");
            }
        }

        protected void btnUptProfileCancel_Click(object sender, EventArgs e)
            {
                Response.Redirect("StudentProfile.aspx");
            }

    }
}
