﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.StudentPackage
{
    public partial class StudentProjectView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            displayProjectList();
        }

        private void displayProjectList()
        {

            // Create a Mentor object.
            Project objProject = new Project();

            // Create a DataSet object to contain the mentee list of a mentor
            DataSet result = new DataSet();

            // Call the getMentorStudent method of Mentor class to retrieve the
            // mentee details of a mentor from database.
            //objProject.studentId = (int)Session["StudentID"];
            objProject.studentId = 2;


            int errorCode = objProject.getProjectStudent(ref result);
            gvProject.DataSource = result.Tables["ProjectDetails"];
            gvProject.DataBind();

            if (errorCode == 0)
            {
                // Load Mentee information to the GridView: gvMentee
                gvProject.DataSource = result.Tables["ProjectDetails"];
                gvProject.DataBind();

                // Display total number of mentee records
                if (result.Tables["ProjectDetails"].Rows.Count > 0)
                    lblMessage.Text = result.Tables["ProjectDetails"].Rows.Count + " projects record(s) found.";
                else
                    lblMessage.Text = "No projects record found.";
            }
            else
            {
                lblMessage.Text = "Smth went wrong and errorcode is not == 0 " + Convert.ToString(errorCode);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentProjectCreate.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("StudentProjectUpdate.aspx");
        }
    }

    
}