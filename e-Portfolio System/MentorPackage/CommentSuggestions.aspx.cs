﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.MentorPackage
{
    public partial class CommentSuggestions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["studentid"] != null)
            {
                // Display StudentID and Name from query strings.
                lblStudentID.Text = Request.QueryString["studentId"];
                lblStudentName.Text = Request.QueryString["name"];
            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            if (Page.IsValid) // check whether all validation controls on a page are currently valid
            {
                int selectedStudentId = Convert.ToInt32(Request.QueryString["studentid"]);

                // Set student portfolio status to 'N'
                // Create a Student object.
                Student student = new Student();
                student.studentId = selectedStudentId;
                student.status = "N";

                // Create a new object from the Suggestion Class
                Suggestion objSuggestion = new Suggestion();

                // Pass data to the properties of the Suggestion object
                objSuggestion.studentId = selectedStudentId;
                objSuggestion.mentorId = (int)Session["MentorID"];
                objSuggestion.description = taText.Value;
                objSuggestion.status = "N";
                objSuggestion.dateCreated = DateTime.Now.ToString();

                // Call the add method to insert the suggestion record to databse.
                int id = objSuggestion.add();

                // Call the update() method of Student class to update a student record,
                // the student ID of the record to be update is passed as a property
                // and whether the method returns an integer errorCode to calling program.
                int errorCode = student.update();

                if (errorCode == 0)
                {
                    // Display appropriate message
                    lblMessage.Text = "Suggestion sent successfully.";

                    // Disable "Yes" and "No" buttons,
                    // display link back to ViewStaff page.
                    lnkReturn.Visible = true;
                }
            }
        }
    }
}