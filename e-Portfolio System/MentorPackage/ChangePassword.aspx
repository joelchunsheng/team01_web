﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ChangePassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .changePassContain {
            width: 1000px;
        }

        .validation {
            color: #FF0000;
        }

        .message {
            margin-left: 80px;
            color: #660066;
        }

        .rowHeader {
            text-align: right;
            font-weight: bold;
            padding: 8px 10px;
        }
        
        .textBox {
            width: 200px;
            font-size: 1em;
            border: none;
            border-bottom: solid 1px #6f1e7f;
            transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 98%, #6f1e7f 4%);
            background-position: -320px 0;
            background-size: 310px 100%;
            background-repeat: no-repeat;
            color: #333;
            font-weight: 500;
        }

        .textBox::-webkit-input-placeholder {
            font-size: 14px;
        }

        .textBox:focus {
            box-shadow: none;
            outline: none;
            background-position: 0 0;
            font-weight: 500;
            border-bottom: solid 2px #6f1e7f;
        }

        .textBox:-webkit-autofill {
            -webkit-box-shadow: 0 0 0 30px white inset;
        }

        .btnSave-row {
            text-align: right;
        }

        .btnSave {
            width: 75px;
            height: 35px;
            background-color: #6f1e7f;
            color: #fff;
            border: none;
            font-weight: 500;
            border-radius: 10px;
            margin-right: 510px;
            margin-top: 10px;

        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">
            CHANGE PASSWORD</p>
        <div class="changePassContain">
            <table class="w-100">
                <tr>
                    <td class="rowHeader">OLD PASSWORD:</td>
                    <td><asp:TextBox ID="txtPassEntered" runat="server" CssClass="textBox" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPassEntered" runat="server" CssClass="validation" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtPassEntered"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="rowHeader">NEW PASSWORD:</td>
                    <td><asp:TextBox ID="txtNewPassword" runat="server" CssClass="textBox" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNewPass" runat="server" CssClass="validation" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtNewPassword"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td class="rowHeader">CONFIRM NEW PASSWORD:</td>
                    <td>
                        <asp:TextBox ID="txtConfirmPass" runat="server" CssClass="textBox" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvConfirmPass" runat="server" CssClass="validation" Display="Dynamic" ErrorMessage="*" ControlToValidate="txtConfirmPass"></asp:RequiredFieldValidator>
                        &nbsp;<asp:CompareValidator ID="covMatch" runat="server" ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPass" CssClass="validation" ErrorMessage="Password does not match above field." ToolTip="Password must be the same." Display="Dynamic"></asp:CompareValidator>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td class="btnSave-row">
                        <asp:Button ID="btnSave" runat="server" Text="SAVE" OnClick="btnSave_Click" CssClass="btnSave" AutoCompleteType="disable"/>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td class="btnSave-row">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblMessage" runat="server" CssClass="message" style="font-weight: 400;"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
