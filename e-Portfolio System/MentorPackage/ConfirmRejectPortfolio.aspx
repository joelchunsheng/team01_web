﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ConfirmRejectPortfolio.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ConfirmRejectPortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: "Segoe UI";
            font-weight: normal;
            text-align: center;
        }
        .auto-style2 {
            font-family: "Segoe UI";
            font-size: x-large;
        }
        .auto-style3 {
            color: #000000;
        }
        .auto-style4 {
            color: #800080;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">
            Reject Student Portfolio</p>
        <p class="auto-style1">
             <span class="auto-style3">
            <asp:Label ID="lblRejectMessage" runat="server"></asp:Label>
            <br />
            <asp:Button ID="btnYes" runat="server" OnClick="btnYes_Click" Text="Yes" />
    &nbsp;
            <asp:Button ID="btnNo" runat="server" OnClick="btnNo_Click" Text="No" />
            <br />
            <br />
            <asp:Label ID="lblMessage" runat="server" CssClass="auto-style4"></asp:Label>
            </span>
            <br />
             <asp:HyperLink ID="lnkReturn" runat="server" NavigateUrl="~/MentorPackage/ViewMenteePortfolios.aspx" Visible="False">Return to View Mentee Portfolios</asp:HyperLink>
        </p>
        </div>
</asp:Content>
