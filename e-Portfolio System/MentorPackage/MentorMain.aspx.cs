﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.MentorPackage
{
    public partial class MentorMain : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["loginId"] != null)
            {
                lblName.Text = (string)Session["Name"];
            }
            else // user not logged in 
            {
                Response.Redirect("~/CommonPages/Login.aspx");
            }
        }
    }
}