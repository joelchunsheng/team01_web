﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.MentorPackage
{
    public partial class ViewIndividualPortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["studentid"] != null)
                {
                    // Create a new Student object
                    Student objStudent = new Student();

                    // Read Student ID from query string
                    objStudent.studentId = Convert.ToInt32(Request.QueryString["studentid"]);

                    // Load student information to controls
                    int errorCode = objStudent.getPortfolioDetails();
                    if (errorCode == 0)
                    {
                        imgPhoto.ImageUrl = "~/Images/" + objStudent.photo; 
                        lblName.Text = objStudent.name;
                        lblDescription.Text = objStudent.description;
                        lblAchievement.Text = objStudent.achievement;
                        lnkLink.NavigateUrl = objStudent.externalLink;
                        lnkLink.Text = objStudent.externalLink;
                        lnkEmail.NavigateUrl = "mailto:" + objStudent.emailAddr;
                        lnkEmail.Text = objStudent.emailAddr;

                        if (objStudent.studentSkillList.Count() > 0)
                        {
                            lblSkill.Text = objStudent.studentSkillList[0].ToString();
                            if (objStudent.studentSkillList.Count() > 1)
                            {
                                for (int i = 1; i < objStudent .studentSkillList.Count; i++)
                                    lblSkill.Text += "; " + objStudent.studentSkillList[i].ToString();
                            }
                        }

                        if (objStudent.status == "Y")
                        {
                            lblStatus.Text = "Released for viewing";
                            btnApprove.Text = "Reject";
                        }
                        else
                        {
                            lblStatus.Text = "Not to be released";
                            btnApprove.Text = "Approve";
                        }
                    } 
                    else if (errorCode == -2)
                    {
                        lblMessage.Enabled = true;
                        lblMessage.Text = "Unable to retrieve portfolio for Student ID " +
                                           objStudent.studentId;
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }

                    if (!checkMentorStudent(objStudent.mentorId))
                    {
                        btnComment.Visible = false;
                        btnApprove.Visible = false;
                    }
                }

                displayProjectList();
            }
        }

        private bool checkMentorStudent(int mentorID)
        {
            int currentMentorID = (int)Session["MentorID"];

            if (mentorID == currentMentorID)
                return true;
            else
                return false;
        }

        protected void btnComment_Click(object sender, EventArgs e)
        {
            Response.Redirect("CommentSuggestions.aspx?name=" + lblName.Text +
                              "&studentid=" + Convert.ToInt32(Request.QueryString["studentid"]));
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            if (btnApprove.Text == "Approve")
            {
                Response.Redirect("ConfirmApprovePortfolio.aspx?name=" + lblName.Text +
                    "&studentid=" + Convert.ToInt32(Request.QueryString["studentid"]));
            }
            else
            {
                Response.Redirect("ConfirmRejectPortfolio.aspx?name=" + lblName.Text +
                    "&studentid=" + Convert.ToInt32(Request.QueryString["studentid"]));
            }

        }

        void displayProjectList()
        {
            // Create a Project object.
            Project objProject = new Project();

            // Create a DataSet object to contain the project list of a student
            DataSet result = new DataSet();

            // Call the  method getProjectStudent of Project class to retrieve the
            // project details of a student from database.
            objProject.studentId = Convert.ToInt32(Request.QueryString["studentid"]);

            int errorCode = objProject.getProjectStudent(ref result);

            if (errorCode == 0)
            {
                // Load Project information to the GridView: gvProject
                gvProject.DataSource = result.Tables["ProjectDetails"];
                gvProject.DataBind();

                // Display total number of mentee records
                if (result.Tables["ProjectDetails"].Rows.Count > 0)
                    lblMessageProj.Text = result.Tables["ProjectDetails"].Rows.Count + " project record(s) found.";
                else
                    lblMessageProj.Text = "No project record found.";
            }
        }
    }
}