﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.MentorPackage
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string passEntered = txtPassEntered.Text;
                string newPass = txtNewPassword.Text;
                bool containsInt = newPass.Any(char.IsDigit);

                if (newPass.Length >= 8 && containsInt == true)
                {
                    int mentorID = (int)Session["MentorID"];

                    string oldPass = "";

                    // Read connection string "Student_EPortfolio" from web.config file.
                    string strConn = ConfigurationManager.ConnectionStrings
                                        ["Student_EPortfolio"].ToString();

                    // Instantiate a SqlConnection object with the Connection String read.
                    SqlConnection conn = new SqlConnection(strConn);

                    // Instantiate a SqlCommand object, supply it with an SQL statement 
                    // and the connection object for connecting to the database.
                    SqlCommand cmd = new SqlCommand
                        ("SELECT * FROM Mentor " +
                        "WHERE MentorID = @selectedMentorId", conn);

                    //Define the parameters used in SQL statement, value for each parameter
                    //is retrieved from respective class's property
                    cmd.Parameters.AddWithValue("@selectedMentorID", mentorID);

                    // Instantiate a DataAdapter object, pass the SqlCommand
                    // object created as parameter.
                    SqlDataAdapter daPassword = new SqlDataAdapter(cmd);

                    // Created a DataSet object result
                    DataSet result = new DataSet();

                    // Open a database connection
                    conn.Open();

                    // Use DataAdapter to fetch data to a table "StudentDetails" in DataSet.
                    daPassword.Fill(result, "PasswordDetails");

                    // Close database connection
                    conn.Close();

                    if (result.Tables["PasswordDetails"].Rows.Count > 0)
                    {
                        // Fill Student object with values from the DataSet
                        DataTable table = result.Tables["PasswordDetails"];
                        if (!DBNull.Value.Equals(table.Rows[0]["Password"]))
                        {
                            oldPass = table.Rows[0]["Password"].ToString();

                            if (oldPass == passEntered)
                            {
                                // Create a Mentor object.
                                Mentor mentor = new Mentor();
                                mentor.mentorId = mentorID;
                                mentor.password = newPass;

                                // Call the update() method of Student class to update a mentor record,
                                // the mentor ID of the record to be update is passed as a property
                                // and whether the method returns an integer errorCode to calling program.
                                int errorCode = mentor.update();

                                if (errorCode == 0)
                                    lblMessage.Text = "Password updated successfully.";
                                else
                                    lblMessage.Text = "Password update unsuccessful. Please try again.";
                            }
                            else
                                lblMessage.Text = "Invalid current password. Password update unsuccessful.";
                        }
                        else
                            lblMessage.Text = "Password update unsuccessful. Please try again.";
                    }
                    else
                        lblMessage.Text = "Password update unsuccessful. Please try again.";
                }
                else
                {
                    lblMessage.Text = "New password must be 8 characters or longer and must contain at least one digit.";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                }
            }
        }

        protected void txtNewPassword_TextChanged(object sender, EventArgs e)
        {

        }
    }
}