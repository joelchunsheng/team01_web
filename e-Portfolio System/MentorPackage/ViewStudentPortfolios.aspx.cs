﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.MentorPackage
{
    public partial class ViewStudentPortfolios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Page is loading onto the web browser, not being refreshed due to
            // "postback" triggered by clicking of a submit button, or other
            // control that causes an auto-postback.
            if (!Page.IsPostBack)
            {
                displayStudentList();
                displaySkillsDropDownList();
            }
        }
        
        private void displayStudentList()
        {
            // Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings
                               ["Student_EPortfolio"].ToString();

            // Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            // Instantiate a SqlCommand object, supply it with a SELECT SQL
            // statement which retrieves all attributes of a student record.
            SqlCommand cmd = new SqlCommand
                        ("SELECT * FROM Student ORDER BY StudentID", conn);

            // Instantiate a DataAdapter object, pass the SqlCommand
            // object created as parameter.
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            // Create a DataSet object to contain the records retrieved from database
            DataSet result = new DataSet();

            // Open a database connection
            conn.Open();

            // Use DataAdapter to fetch data to a table "StudentDetails" in DataSet.
            daStudent.Fill(result, "StudentDetails");

            // Close database connection
            conn.Close();

            // Specify GridView to get data from table "StudentDetails"
            // in DataSet "result"
            gvStudent.DataSource = result.Tables["StudentDetails"];

            // Display the list of data in GridView
            gvStudent.DataBind(); 

            // Display total number of staff records
            if (result.Tables["StudentDetails"].Rows.Count > 0)
                lblMessage.Text = ": " + result.Tables["StudentDetails"].Rows.Count + " student record(s) found.";
            else
                lblMessage.Text = "No student record found.";
        }

        private void displaySkillsDropDownList()
        {
            // Read connection string "NPBookConnectionString" from web.config file
            string strConn = ConfigurationManager.ConnectionStrings
                               ["Student_EPortfolio"].ToString();

            // Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            // Instantiate a SqlCommand object, supply it with a SELECT SQL
            // statement which retrieves all attributes of a skill record.
            SqlCommand cmd = new SqlCommand
                        ("SELECT * FROM SkillSet", conn);

            // Instantiate a DataAdapter object, pass the SqlCommand
            // object created as parameter.
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            // Create a DataSet object to contain the records retrieved from database
            DataSet result = new DataSet();

            // Open a database connection
            conn.Open();

            // Use DataAdapter to fetch data to a table "SkillDetails" in DataSet.
            daStudent.Fill(result, "SkillDetails");

            // Close database connection
            conn.Close();

            ////////////////////////////////////////////////////////////////////////////////////////

            // Specify the dropdown list to get data from the dataset
            ddlSkills.DataSource = result.Tables["SkillDetails"];

            // Specify the Text property of dropdownlist
            ddlSkills.DataTextField = "SkillSetName";

            // Specify the Value property of dropdownlist
            ddlSkills.DataValueField = "SkillSetID";

            // Load Skill information to the drop-down list
            ddlSkills.DataBind();

            // Insert prompt for the DropDownList
            ddlSkills.Items.Insert(0, "-- Select --");

            ////////////////////////////////////////////////////////////////////////////////////////

            // Specify the dropdown list to get data from the dataset
            ddlSkills2.DataSource = result.Tables["SkillDetails"];

            // Specify the Text property of dropdownlist
            ddlSkills2.DataTextField = "SkillSetName";

            // Specify the Value property of dropdownlist
            ddlSkills2.DataValueField = "SkillSetID";

            // Load Skill information to the drop-down list
            ddlSkills2.DataBind();

            // Insert prompt for the DropDownList
            ddlSkills2.Items.Insert(0, "-- Select --");
        }

        protected void ddlSkills_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSkills.SelectedIndex != 0)
            {
                ddlSkills2.Enabled = true;
            }
            else
            {
                ddlSkills2.SelectedIndex = 0;
                ddlSkills2.Enabled = false;
            }

            // Get the SelectedIndex from the DropDownList
            int selectedSkillSetId = Convert.ToInt32(ddlSkills.SelectedIndex);

            if (selectedSkillSetId > 0)
            {
                // Create a SkillSet object.
                SkillSet objSkillSet = new SkillSet();

                // Create a DataSet object to contain the student list of a skill
                DataSet result = new DataSet();

                // Call the getSkillStudent method of Skill class to retrieve the
                // student details of a skill from database.
                objSkillSet.skillSetId = selectedSkillSetId;

                int errorCode = objSkillSet.getSkillStudent(ref result);

                if (errorCode == 0)
                {
                    // Load Staff information to the GridView: gvStudent
                    gvStudent.DataSource = result.Tables["StudentDetails"];
                    gvStudent.DataBind();
                }
            }
            else // if user selects (--Select--)
            {
                displayStudentList();
            }
        }

        protected void ddlSkills2_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get the SelectedIndex from the DropDownList
            int selectedSkillSetId = Convert.ToInt32(ddlSkills.SelectedIndex);
            int selectedSkillSetId2 = Convert.ToInt32(ddlSkills2.SelectedIndex);

            if (selectedSkillSetId2 > 0)
            {
                // Create a SkillSet object.
                SkillSet objSkillSet = new SkillSet();

                // Call the getSkillStudent method of Skill class to retrieve the
                // student details of a skill from database.
                objSkillSet.skillSetId = selectedSkillSetId;
                objSkillSet.skillSetId2 = selectedSkillSetId2;

                if (selectedSkillSetId == selectedSkillSetId2)
                {
                    if (selectedSkillSetId > 0)
                    {
                        // Create a DataSet object to contain the student list of a skill
                        DataSet result = new DataSet();

                        // Call the getSkillStudent method of Skill class to retrieve the
                        // student details of a skill from database.
                        objSkillSet.skillSetId = selectedSkillSetId;

                        int errorCode1 = objSkillSet.getSkillStudent(ref result);

                        if (errorCode1 == 0)
                        {
                            // Load Staff information to the GridView: gvStudent
                            gvStudent.DataSource = result.Tables["StudentDetails"];
                            gvStudent.DataBind();
                        }
                    }
                }
                else
                {
                    // Create a DataSet object to contain the student list of a skill
                    DataSet result = new DataSet();

                    int errorCode = objSkillSet.getTwoSkillStudent(ref result);

                    if (errorCode == 0)
                    {
                        // Load Staff information to the GridView: gvStudent
                        gvStudent.DataSource = result.Tables["SpecifiedStudentDetails"];
                        gvStudent.DataBind();
                    }
                }
            }

            else // if user selects (--Select--)
            {
                if (selectedSkillSetId > 0)
                {
                    // Create a SkillSet object.
                    SkillSet objSkillSet = new SkillSet();

                    // Create a DataSet object to contain the student list of a skill
                    DataSet result = new DataSet();

                    // Call the getSkillStudent method of Skill class to retrieve the
                    // student details of a skill from database.
                    objSkillSet.skillSetId = selectedSkillSetId;

                    int errorCode = objSkillSet.getSkillStudent(ref result);

                    if (errorCode == 0)
                    {
                        // Load Staff information to the GridView: gvStudent
                        gvStudent.DataSource = result.Tables["StudentDetails"];
                        gvStudent.DataBind();
                    }
                }
            }
        }

        protected void gvStudent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Set the page index to the page clicked by user
            gvStudent.PageIndex = e.NewPageIndex;
            // Display records on the new page.
            displayStudentList();
        }

        public string displayYorN(string status)
        {
            if (status == "Y")
                return "Approved";
            else
                return "Not Approved";
        }
    }
}