﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.MentorPackage
{
    public partial class ViewProject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["projectid"] != null && Request.QueryString["studentid"] != null)
                {
                    // Create a new Project object
                    Project objProject = new Project();

                    // Read Student ID from query string
                    objProject.projectId = Convert.ToInt32(Request.QueryString["projectid"]);
                    objProject.studentId = Convert.ToInt32(Request.QueryString["studentid"]);

                    // Load student information to controls
                    int errorCode = objProject.getProjectDetails();
                    if (errorCode == 0)
                    {
                        imgPoster.ImageUrl = "~/Images/Projects/" + objProject.projectPoster;
                        lblProjectName.Text = objProject.title;
                        lblDescription.Text = objProject.description;
                        lnkUrl.Text = objProject.projectUrl;
                        lnkUrl.NavigateUrl = objProject.projectUrl;
                        lblRole.Text = objProject.role;
                        lblReflection.Text = objProject.reflection;

                        lnkReturn.NavigateUrl = "~/MentorPackage/ViewIndividualPortfolio.aspx?studentid=" + Convert.ToInt32(Request.QueryString["studentid"]);
                    }
                    else if (errorCode == -2)
                    {
                        lblMessage.Enabled = true;
                        lblMessage.Text = "Unable to retrieve project details for Student ID " +
                                           objProject.studentId;
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
        }
    }
}