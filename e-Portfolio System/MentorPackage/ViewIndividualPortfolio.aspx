﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ViewIndividualPortfolio.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ViewIndividualPortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .container-all {
            margin-left: 55px;
            margin-right: 55px;
            margin-bottom: 40px;
        }

        .image{
            max-width: 200px;
            max-height: 200px;
        }

        .student-name {
            font-size: medium; 
            font-weight: 700; 
            color: #660066;
            font-size: 20px;
            margin-left: 30px;
        }

        .row-title {
            font-weight: bold;
        }

        .row-title, .row-style {
            width: 200px;
        }

        tr, td {
            padding: 10px;            
        }

        .btn-row {
            text-align: right;
        }

        .btn-row .btnAll {
            width: 120px;
            height: 40px;
            background-color: #6f1e7f;
            color: #fff;
            border: none;
            font-weight: 500;
            border-radius: 10px;
        }
        .auto-style1 {
            font-weight: bold;
            height: 62px;
        }
        .auto-style2 {
            height: 62px;
        }
        .auto-style3 {
            color: #660066;
        }

        .label {
            font-weight: 400;
            font-style: italic;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <table class="w-100">
            <tr class="row-style">
                <td>
                    <asp:Image ID="Image1" runat="server" Width="0px" />
                    <asp:Image ID="imgPhoto" runat="server" class="image" />
                </td>
                <td>
                    <asp:Label ID="lblName" runat="server" CssClass="student-name"></asp:Label>
                    <br />
                    <asp:Label ID="lblMessage" runat="server" Enabled="False"></asp:Label>
                </td>
            </tr>
            <tr class="row-style">
                <td class="row-title">DESCRIPTION</td>
                <td>
                    <asp:Label ID="lblDescription" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="row-style">
                <td class="row-title">SKILLS</td>
                <td>
                    <asp:Label ID="lblSkill" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="row-style">
                <td class="row-title">ACHIEVEMENTS</td>
                <td>
                    <asp:Label ID="lblAchievement" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="row-style">
                <td class="row-title">LINKS</td>
                <td>
                    <asp:HyperLink ID="lnkLink" runat="server" Target="_blank">[lnkLink]</asp:HyperLink>
                </td>
            </tr>
            <tr class="row-style">
                <td class="auto-style1">EMAIL ADDRESS</td>
                <td class="auto-style2">
                    <asp:HyperLink ID="lnkEmail" runat="server">[lnkEmail]</asp:HyperLink>
                </td>
            </tr>
            <tr class="row-style">
                <td class="row-title">PORTFOLIO STATUS</td>
                <td>
                    <asp:Label ID="lblStatus" runat="server"></asp:Label>
                </td>
            </tr>
            <tr class="row-style">
                <td class="row-title">PROJECTS<br /><asp:Label ID="lblMessageProj" runat="server" Text="" CssClass="auto-style3 label"></asp:Label>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr class="row-style">
                <td class="row-title" colspan="2">
                    <asp:GridView ID="gvProject" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="15" GridLines="Horizontal" Width="100%" Height="308px">
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                        <Columns>
                            <asp:HyperLinkField DataNavigateUrlFields="ProjectID,StudentID" DataNavigateUrlFormatString="ViewProject.aspx?projectid={0}&amp;studentid={1}" DataTextField="ProjectID" HeaderText="ID" />
                            <asp:BoundField DataField="Title" HeaderText="Title" />
                            <asp:BoundField DataField="Description" HeaderText="Description" />
                            <asp:ImageField DataImageUrlField="ProjectPoster" DataImageUrlFormatString="~/Images/Projects/{0}" HeaderText="Poster">
                                <ControlStyle Height="150px"/>
                            </asp:ImageField>
                            <asp:HyperLinkField DataTextField="ProjectURL" HeaderText="Project URL" DataTextFormatString="{0}" />
                        </Columns>
                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <SortedAscendingCellStyle BackColor="#F4F4FD" />
                        <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                        <SortedDescendingCellStyle BackColor="#D8D8F0" />
                        <SortedDescendingHeaderStyle BackColor="#3E3277" />
                    </asp:GridView>
                </td>
            </tr>
            <tr class="row-style">
                <td></td>
                <td class="btn-row">
                    <asp:Button ID="btnComment" runat="server" Text="Comment" OnClick="btnComment_Click" CssClass="btnAll"/>    &nbsp;&nbsp;
                    <asp:Button ID="btnApprove" runat="server" Text="Approve" OnClick="btnApprove_Click" CssClass="btnAll"/>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
