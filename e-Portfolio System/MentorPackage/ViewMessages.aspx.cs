﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.MentorPackage
{
    public partial class ViewMessages : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Page is loading onto the web browser, not being refreshed due to
            // "postback" triggered by clicking of a submit button, or other
            // control that causes an auto-postback. 
            if (!Page.IsPostBack)
                displayMessages();
        }

        private void displayMessages()
        {
            // Create a Message object.
            Message objMessage = new Message();

            // Create a DataSet object to contain the message list for the mentor
            DataSet result = new DataSet();

            // Call the getMessageDetails method of Message class to retrieve the
            // message details for a mentor from database.
            objMessage.toId = (int)Session["MentorID"];

            int errorCode = objMessage.getMessageDetails(ref result);

            if (errorCode == 0)
            {
                // Load Staff information to the GridView: gvMessages
                gvMessages.DataSource = result.Tables["MessageDetails"];
                gvMessages.DataBind();

                // Display total number of message records
                if (result.Tables["MessageDetails"].Rows.Count > 0)
                    lblMessage.Text = result.Tables["MessageDetails"].Rows.Count + " message record(s) found.";
                else
                    lblMessage.Text = "No message record found.";
            }
        }

        protected void gvMessages_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Set the page index to the page clicked by user
            gvMessages.PageIndex = e.NewPageIndex;
            // Display records on the new page.
            displayMessages();
        }
    }
}