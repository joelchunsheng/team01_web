﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="MentorMain.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.MentorMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #option {
            font-weight: 500;
            color: #000;
            font-size: 16px;
            letter-spacing: 1px;
            margin-top: -5px;
        }

        .imageButtons {
            width: 1200px;
            text-align: center;
            margin: 0 auto;
            margin-top: 40px;
        }

        .text-below {
            text-align:center;
            font-weight: 700; 
            color: #660066;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">
            Welcome,
            <asp:Label ID="lblName" runat="server" CssClass="auto-style1"></asp:Label>!
        </p>
        <p id="option">
            Would you like to...
        </p>
        <div>
            <div class="imageButtons">
                <asp:ImageButton ID="imgMenteePortfolios" runat="server" Width="200px" style="padding: 40px" ImageUrl="~/Images/Assets/Mentor/001-mentee.png" PostBackUrl="~/MentorPackage/ViewMenteePortfolios.aspx"/>
                <asp:ImageButton ID="imgStudentPortfolios" runat="server" Width="200px" style="padding: 40px" ImageUrl="~/Images/Assets/Mentor/002-student.png" PostBackUrl="~/MentorPackage/ViewStudentPortfolios.aspx"/>
                <asp:ImageButton ID="imgEMessagesBoard" runat="server" Width="200px" style="padding: 40px" ImageUrl="~/Images/Assets/Mentor/003-message.png" PostBackUrl="~/MentorPackage/ViewMessages.aspx"/>
                <asp:ImageButton ID="imgSearch" runat="server" Width="200px" style="padding: 40px" ImageUrl="~/Images/Assets/Mentor/004-search.png" PostBackUrl="~/MentorPackage/ViewStudentPortfolios.aspx"/>
                <asp:ImageButton ID="imgChangePassword" runat="server" Width="200px" style="padding: 40px" ImageUrl="~/Images/Assets/Mentor/005-password.png" PostBackUrl="~/MentorPackage/ChangePassword.aspx"/>
            </div>
            <div class="text-below">
                <div style="display:inline-block; margin: 0 35px; max-width: 130px; ">
                    view mentee portfolios
                </div>
                <div style="display:inline-block; margin: 0 35px; max-width: 130px;">
                    view all portfolios
                </div>
                <div style="display:inline-block; margin: 0 40px; max-width: 130px;">
                    read & reply messages
                </div>
                <div style="display:inline-block; margin: 0 35px; max-width: 130px;">
                    search for a student
                </div>
                <div style="display:inline-block; margin: 0 35px; max-width: 130px;">
                    change password
                </div>
            </div>
        </div>
    </div>
</asp:Content>