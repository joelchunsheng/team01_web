﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.MentorPackage
{
    public partial class ConfirmApprovePortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Check if there is a query string "studentid" in the URL
                if (Request.QueryString["studentid"] != null)
                {
                    // Get the student ID from query string,
                    // and convert the query string to 32-bit integer.
                    int studentID = Convert.ToInt32(Request.QueryString["studentid"]);
                    string studentName = Request.QueryString["name"];

                    // Create a Student object.
                    Student student = new Student();
                    student.status = "Y";
                    student.studentId = studentID;

                    // Call the update() method of Student class to update a student record,
                    // the student ID of the record to be update is passed as a property
                    // and whether the method returns an integer errorCode to calling program.
                    int errorCode = student.update();

                    if (errorCode == 0)
                    {
                        // Display successful message
                        lblApproveMessage.Text = "Student " + Request.QueryString["studentId"] + " - " + Request.QueryString["name"] + "'s portfolio has been successfully approved.";
                        lnkReturn.Visible = true;
                    }
                    else
                    {
                        // Display error message
                        lblApproveMessage.Text = "Approval request unsuccessful. Please try again.";
                        lnkReturn.Visible = true;
                    }
                }
            }
        }
    }
}