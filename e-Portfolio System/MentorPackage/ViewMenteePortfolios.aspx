﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ViewMenteePortfolios.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ViewMenteePortfolios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">MENTEE PORTFOLIOS</p>
        <p>
            View Mentee Portfolios:
            <asp:Label ID="lblMessage" runat="server" style="font-family: 'Segoe UI'; font-weight: 400; font-style: italic; font-size: medium;"></asp:Label>
        </p>
        <p style="font-weight: 400;">
            Click on a mentee's ID to view their portfolio, give suggestions for improvement and approve their portfolio for public viewing.
        </p>
        <p>
            <asp:GridView ID="gvMentee" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" GridLines="Horizontal" Width="100%" OnPageIndexChanging="gvMentee_PageIndexChanging" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px">
                <AlternatingRowStyle BackColor="#FDFDFD" />
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="StudentID" DataNavigateUrlFormatString="ViewIndividualPortfolio.aspx?studentid={0}" DataTextField="StudentID" HeaderText="Mentee ID" />
                    <asp:BoundField DataField="Name" HeaderText="Mentee Name" />
                    <asp:ImageField DataImageUrlField="Photo" DataImageUrlFormatString="~/Images/{0}" HeaderText="Photo">
                        <ControlStyle Height="70px"/>
                    </asp:ImageField>
                    <asp:BoundField DataField="Course" HeaderText="Course" />
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# displayYorN(Convert.ToString(Eval("Status"))) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="StudentID,Name" DataNavigateUrlFormatString="ConfirmApprovePortfolio.aspx?studentid={0}&amp;name={1}" Text="Approve" />
                    <asp:HyperLinkField DataNavigateUrlFields="StudentID,Name" DataNavigateUrlFormatString="ConfirmRejectPortfolio.aspx?studentid={0}&amp;name={1}" Text="Reject" />
                </Columns>
                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                <SortedDescendingHeaderStyle BackColor="#3E3277" />
            </asp:GridView>
        </p>
    </div>
</asp:Content>
