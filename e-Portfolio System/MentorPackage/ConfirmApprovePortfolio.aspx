﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ConfirmApprovePortfolio.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ConfirmApprovePortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: "Segoe UI";
            font-weight: normal;
            text-align: center;
        }
        .auto-style2 {
            font-family: "Segoe UI";
            font-size: x-large;
        }
        .auto-style3 {
            color: #000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">
            Approve Student Portfolio</p>
        <p class="auto-style1">
            <asp:Label ID="lblApproveMessage" runat="server"></asp:Label>
            <br />
            <asp:HyperLink ID="lnkReturn" runat="server" NavigateUrl="~/MentorPackage/ViewMenteePortfolios.aspx" Visible="False">Return to View Mentee Portfolios</asp:HyperLink>
            <br />
            <br />
        </p>
    </div>
</asp:Content>
