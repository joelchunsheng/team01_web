﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="CommentSuggestions.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.CommentSuggestions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: "Segoe UI";
            font-size: x-large;
        }
        .centeralign {
            text-align: center;
        }
        #lblMessage, #lnkReturn {
            font-weight: normal;
        }
        .auto-style2 {
            font-family: "Segoe UI";
            font-weight: normal;
        }
        #centeralign {
            margin: 0 auto;
            width: 1000px;
        }
        .auto-style3 {
            width: 1000px;
            height: 300px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">
            Suggestions to Improve Portfolio</p>
        <div id="centeralign">
            <p>
                <span class="auto-style2">to:
                <strong>
                <asp:Label ID="lblStudentName" runat="server"></asp:Label>
                </strong>&nbsp;<em>( Student Number
                <asp:Label ID="lblStudentID" runat="server"></asp:Label>
                )
            </em></span>
            <br />
                <textarea required id="taText" class="auto-style3" name="S1" runat="server"></textarea><br />
                <asp:Button ID="btnSend" runat="server" Text="Send" OnClick="btnSend_Click" Width="100px" style="margin-left:900px;" />
            <br />
            </p>
        </div>
        <p class="text-center">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <br />
            <asp:HyperLink ID="lnkReturn" runat="server" Visible="False" NavigateUrl="ViewMenteePortfolios.aspx">Return to Mentee Portfolios</asp:HyperLink>
        </p>
    </div>
</asp:Content>
 