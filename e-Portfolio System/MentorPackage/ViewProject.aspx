﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ViewProject.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ViewProject" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .container-all {
            margin-left: 55px;
            margin-right: 55px;
            margin-bottom: 50px;
        }

        .title {
            font-weight: bold;
            font-size: 1.5em;
        }

        .sub-title {
            font-weight: bold;
            font-size: 1.3em;
        }

        .column1 {
            width: 900px;
        }

        td, tr {
            padding-right: 50px;
        }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">
            Student Projects</p>
        <p>
            <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
        </p>
        <table class="w-100">
            <tr class="sub-title">
                <td class="column1">
            Project Title:
            <asp:Label ID="lblProjectName" runat="server"></asp:Label>
                </td>
                <td rowspan="7">
                    <asp:Image ID="imgPoster" runat="server" Height="600px" />
                </td>
            </tr>
            <tr>
                <td class="column1">
            <asp:Label ID="lblDescription" runat="server"></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="column1"><strong>URL:
                    </strong>
                    <asp:HyperLink ID="lnkUrl" runat="server">[lnkUrl]</asp:HyperLink>
                    </td>
            </tr>
            <tr>
                <td class="column1"><strong>Role of Student: </strong>
                    <asp:Label ID="lblRole" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="column1"><strong>Reflection:</strong><br />
                <asp:Label ID="lblReflection" runat="server"></asp:Label>
            </tr>
            <tr>
                <td class="column1">
                    <asp:HyperLink ID="lnkReturn" runat="server">Return to View Mentee Portfolios</asp:HyperLink>
                    <br />
                    <asp:Label ID="lblMessage" runat="server" Enabled="False"></asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
