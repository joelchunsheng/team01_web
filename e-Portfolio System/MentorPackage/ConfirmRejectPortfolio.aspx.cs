﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.MentorPackage
{
    public partial class ConfirmRejectPortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblRejectMessage.Text = "Do you wish to reject Student " + Request.QueryString["studentId"] + " - " + Request.QueryString["name"] + "'s portfolio?";
        }

        protected void btnYes_Click(object sender, EventArgs e)
        {
            // Check if there is a query string "studentid" in the URL
            if (Request.QueryString["studentid"] != null)
            {
                // Create a Student object.
                Student student = new Student();
                student.studentId = Convert.ToInt32(Request.QueryString["studentid"]);
                student.status = "N";

                // Call the update() method of Student class to update a student record,
                // the student ID of the record to be update is passed as a property
                // and whether the method returns an integer errorCode to calling program.
                int errorCode = student.update();

                if (errorCode == 0)
                {
                    // Display appropriate message
                    lblMessage.Text = "Portfolio status updated successfully to 'Not Approved'.";

                    // Disable "Yes" and "No" buttons,
                    // display link back to View Mentee Portfolio page.
                    btnYes.Enabled = false;
                    btnNo.Enabled = false;
                    lnkReturn.Visible = true;
                }
            }
        }

        protected void btnNo_Click(object sender, EventArgs e)
        {
            // return to "ViewMenteePortfolios" page.
            Response.Redirect("ViewMenteePortfolios.aspx");
        }
    }
}