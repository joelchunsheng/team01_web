﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.MentorPackage
{
    public partial class ViewIndividualMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["messageid"] != null)
                {
                    // Create a new Message object
                    Message objMessage = new Message();

                    DataSet result = new DataSet();

                    // Read Message ID from query string and current Mentor ID
                    objMessage.messageId = Convert.ToInt32(Request.QueryString["messageid"]);
                    objMessage.toId = (int)Session["MentorID"];

                    // Load student information to controls
                    int errorCode = objMessage.getMessageDetails(ref result);
                    if (errorCode == 0)
                    {
                        lblTitle.Text = objMessage.title;
                        lblName.Text = objMessage.parentName;
                        lblReplyTo.Text = objMessage.parentName;
                        lblDatePosted.Text = objMessage.dateTimePosted.ToString();
                        lblText.Text = objMessage.text;

                        displayReplies();
                        //toAppendReply();
                    }
                    else if (errorCode == -2)
                    {
                        lblMessage.Text = "Unable to retrieve message details from Parent " +
                                           objMessage.parentName;
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
        }

        private void displayReplies()
        {
            // Create a Reply object.
            Reply objReply = new Reply();

            // Create a DataSet object to contain the reply list for the message
            DataSet result = new DataSet();

            // Call the getMessageDetails method of Message class to retrieve the
            // reply details for a message from database.
            objReply.messageId = Convert.ToInt32(Request.QueryString["messageid"]);

            int errorCode = objReply.getReplyDetails(ref result);

            if (errorCode == 0)
            {
                // Load Reply information to the GridView: gvReply
                gvReply.Visible = true;
                gvReply.DataSource = result.Tables["ReplyDetails"];
                gvReply.DataBind();
            }
        }

        //public void toAppendReply()
        //{
        //    //Create Message object
        //    Message objMessage = new Message();
        //    objMessage.messageId = Convert.ToInt32(Request.QueryString["messageid"]);
        //    objMessage.toId = (int)Session["MentorID"];

        //    DataSet resultMessage = new DataSet();

        //    // Load student information
        //    objMessage.getMessageDetails(ref resultMessage);

        //    //////////////////////////////////////////////////////////////////////////////////////////

        //    // Create Reply object
        //    Reply objReply = new Reply();
        //    objReply.messageId = Convert.ToInt32(Request.QueryString["messageid"]);

        //    DataSet resultReply = new DataSet();

        //    // Load student information to controls
        //    int errorCodeReply = objReply.getReplyDetails(ref resultReply);
        //    string personReplied = "";
        //    if (errorCodeReply == 0)
        //    {
        //        // Find whether it is parent or mentor who replied.
        //        string whoReplied = objReply.whoReplied;

        //        if (whoReplied == "Parent")
        //        {
        //            personReplied = objReply.parentName;
        //        }
        //        else
        //        {
        //            personReplied = objReply.mentorName;
        //        }
        //    }

        //    //////////////////////////////////////////////////////////////////////////////////////////

        //    appendReply.InnerHtml +=
        //        "<p class=\"auto-style2\">" +
        //        "<span class=\"auto-style3\">TITLE: Re: </span>" +
        //        "<asp:Label ID = \"lblReplyTitle\" runat=\"server\" CssClass=\"auto-style3\">" + objMessage.title + "</asp:Label></p>" +
        //        "<p>Replied by <asp:Label ID = \"lblReplyName\" runat= \"server\" >" + personReplied + "</ asp:Label> at <asp:Label ID=\"lblDateReplied\" runat=\"server\">" + objReply.dateTimePosted + "</asp:Label>.</p>" +
        //        "<p>Message:<br /><asp:Label ID = \"lblReplyText\" runat= \"server\" style= \"font-weight:400;\" >" + objReply.text + "</ asp:Label></p>" +
        //        "<hr />";
        //}

        protected void btnReply_Click(object sender, EventArgs e)
        {
            if (Page.IsValid) // check whether all validation controls on a page are currently valid
            {
                int messageId = Convert.ToInt32(Request.QueryString["messageid"]);
                int parentId = Convert.ToInt32(Request.QueryString["parentid"]);

                // Create a Reply object.
                Reply objReply = new Reply();

                // Pass data to the properties of the Reply object
                objReply.messageId = messageId;
                objReply.mentorId = (int)Session["MentorID"];
                objReply.parentId = parentId;
                objReply.dateTimePosted = DateTime.Now.ToString();
                objReply.text = taReplyMessage.Value;

                // Call the add method to insert the reply record to database.
                int id = objReply.add();

                lblMessage.Text = "Reply sent successfully.";
                displayReplies();
                //toAppendReply();
            }
        }
    }
}

