﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;



namespace e_Portfolio_System.MentorPackage
{
    public partial class ViewMenteePortfolios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Page is loading onto the web browser, not being refreshed due to
            // "postback" triggered by clicking of a submit button, or other
            // control that causes an auto-postback.
            if (!Page.IsPostBack)
                displayMenteeList();
        }

        private void displayMenteeList()
        {
            // Create a Mentor object.
            Mentor objMentor = new Mentor();

            // Create a DataSet object to contain the mentee list of a mentor
            DataSet result = new DataSet();

            // Call the getMentorStudent method of Mentor class to retrieve the
            // mentee details of a mentor from database.
            objMentor.mentorId = (int)Session["MentorID"];

            int errorCode = objMentor.getMentorStudent(ref result);

            if (errorCode == 0)
            {
                // Load Mentee information to the GridView: gvMentee
                gvMentee.DataSource = result.Tables["MenteeDetails"];
                gvMentee.DataBind();

                // Display total number of mentee records
                if (result.Tables["MenteeDetails"].Rows.Count > 0)
                    lblMessage.Text = result.Tables["MenteeDetails"].Rows.Count + " mentee record(s) found.";
                else
                    lblMessage.Text = "No mentee record found.";
            }
        }

        protected void gvMentee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Set the page index to the page clicked by user
            gvMentee.PageIndex = e.NewPageIndex;
            // Display records on the new page.
            displayMenteeList();
        }

        public string displayYorN(string status)
        {
            if (status == "Y")
                return "Approved";
            else
                return "Not Approved";
        }
    }
}