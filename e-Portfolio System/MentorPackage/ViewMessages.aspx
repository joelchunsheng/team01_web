﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ViewMessages.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ViewMessages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: "Segoe UI";
            font-size: x-large;
        }
        .auto-style2 {
            font-family: "Segoe UI";
            font-weight: normal;
            color: #000000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">
            e-Messages Board</p>
        <p>
            View Messages:
            <asp:Label ID="lblMessage" runat="server" style="font-family: 'Segoe UI'; font-weight: 400; font-style: italic; font-size: medium;"></asp:Label>
        </p>
        <p style="font-weight: 400;">
            Click on a message to view and make replies.</p>
        <p class="auto-style2">
            <asp:GridView ID="gvMessages" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="5" GridLines="Horizontal" Width="100%" AllowPaging="True" OnPageIndexChanging="gvMessages_PageIndexChanging">
                <AlternatingRowStyle BackColor="#F7F7F7" />
                <Columns>
                    <asp:HyperLinkField DataNavigateUrlFields="MessageID" DataNavigateUrlFormatString="ViewIndividualMessage.aspx?messageid={0}" DataTextField="MessageID" HeaderText="Message ID" />
                    <asp:BoundField DataField="Title" HeaderText="Title" />
                    <asp:BoundField DataField="ParentName" HeaderText="Posted By" />
                    <asp:BoundField DataField="DateTimePosted" HeaderText="Date" />
                </Columns>
                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                <SortedDescendingHeaderStyle BackColor="#3E3277" />
            </asp:GridView>
        </p>
    </div>
</asp:Content>
