﻿ <%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ViewIndividualMessage.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ViewIndividualMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-family: "Segoe UI";
            font-size: x-large;
        }
        .auto-style2 {
            font-family: "Segoe UI";
        }
        .auto-style3 {
            font-size: large;
        }
        .auto-style4 {
            width: 1000px;
            height: 250px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
        <p class="title">VIEW MESSAGE</p>

        <div id="messages">
            <p class="auto-style2">
            <span class="auto-style3">TITLE: </span>
            <asp:Label ID="lblTitle" runat="server" CssClass="auto-style3"></asp:Label>
        </p>
        <p>
            Posted by <asp:Label ID="lblName" runat="server"></asp:Label>
            at <asp:Label ID="lblDatePosted" runat="server"></asp:Label>
            .</p>
        <p>
            Message:<br /><asp:Label ID="lblText" runat="server" style="font-weight:400;"></asp:Label>
        </p>
        <hr />
        </div>

        <div runat="server" id="appendReply">
        </div>

        <p id="replyTable">
            Reply to:
            <asp:Label ID="lblReplyTo" runat="server"></asp:Label>
            <br />Message:<br />
            <textarea required id="taReplyMessage" class="auto-style4" name="S1" runat="server"></textarea><br />
        </p>
        <p>
            <asp:Button ID="btnReply" runat="server" Text="Reply" Width="135px" OnClick="btnReply_Click" />
        </p>
        <p>
            <asp:Label ID="lblMessage" runat="server" style="font-weight:400;"></asp:Label>
        </p>
        <hr /><br />
        <p>
            <asp:GridView ID="gvReply" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" Visible="False" Width="1000px">
                <AlternatingRowStyle BackColor="#F7F7F7" />
                <Columns>
                    <asp:BoundField DataField="ReplyID" HeaderText="Reply ID" >
                
                    <HeaderStyle Width="100px" />
                    </asp:BoundField>
                
                    <asp:BoundField DataField="RepliedBy" HeaderText="Replied By">
                    <HeaderStyle Width="100px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DateTimePosted" HeaderText="Date Posted" >
                    <HeaderStyle Width="200px" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Text" HeaderText="Reply Message" />
                </Columns>
                <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                <SortedAscendingCellStyle BackColor="#F4F4FD" />
                <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                <SortedDescendingCellStyle BackColor="#D8D8F0" />
                <SortedDescendingHeaderStyle BackColor="#3E3277" />
            </asp:GridView>
            <br />
        </p>
    </div>
</asp:Content>
