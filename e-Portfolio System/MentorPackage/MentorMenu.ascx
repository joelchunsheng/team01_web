﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MentorMenu.ascx.cs" Inherits="e_Portfolio_System.MentorPackage.MentorMenu" %>

<style>
    nav {
        font-size: 14px;
        background-color: #fff;
        opacity: 1;
    }

    nav a:first-child {
        margin-left: 30px;
    }

    #firstNavItem {
        margin-left: 30px;
    }

    #mentorNavbar, #logo {
        margin-top: 10px;
    }

    #mentorNavbar a {
        display: inline-block;
        padding: 25px 10px 0 20px;
        text-transform: uppercase;
        text-decoration: none;
        color: #7c7c7c; 
        font-weight: 500;
        border-left: 1.5px solid #7c7c7c;
        cursor: pointer;
    }

    #mentorNavbar a:hover {
        color: #6f1e7f;
        border-left: 1.5px solid #6f1e7f;
    }

    .btnLogOut {
        margin-right: 50px;
        border-radius: 0;
        padding: 25px 10px 0 20px;
        font-size: 14px;
        color: #7c7c7c;
        font-weight: 500;
        border-left: 1.5px solid #7c7c7c;
        margin-left: 30px;
    }

    .btnLogOut:hover {
        color: #6f1e7f;
        border-left: 1.5px solid #6f1e7f;
        text-decoration: none;
    }

    nav.change {
        background: #fafafa;
        transition: all ease .5s;
    }

    .active {
        color: #6f1e7f;
        border-left: 1.5px solid #6f1e7f;
    }
</style>


<!-- A grey navbar that expands horizontally at medium device -->
<nav class="navbar fixed-top navbar-expand-md navbar-light">
    
    <!-- The brand (or icon) of the navbar -->
    <a class="navbar-brand"  href="MentorMain.aspx" id="logo">
        <img src="../Images/Assets/ict_logo.png" alt="ICT Logo" width="55"/>
    </a>
 
    <!-- Toggle/collapsible Button, also known as hamburger button -->
    <button class="navbar-toggler" type="button"
        data-toggle="collapse" data-target="#mentorNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <!-- Links in the navbar, displayed as drop-down list when collapsed -->
    <div class="collapse navbar-collapse" id="mentorNavbar">
        <!-- Links that are aligned to the left, mr-auto: right margin auto-adjusted -->
        <ul class="navbar-nav mr-auto nav">
            <li class="nav-item">
                <a class="nav-link" href="ViewMenteePortfolios.aspx">MENTEE PORFOLIOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ViewStudentPortfolios.aspx">STUDENT PORTFOLIOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ViewMessages.aspx">E-MESSAGES BOARD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ChangePassword.aspx">CHANGE PASSWORD</a>
            </li>
        </ul>
 
        <!-- Links that are aligned to the right, ml-auto: left margin auto-adjusted -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <!-- A Web Form Control button for logging out user -->
                <asp:Button ID="btnLogOut" runat="server" Text="LOG OUT"
                    CssClass="btn btn-link btnLogOut" CausesValidation="False" OnClick="btnLogOut_Click" />
            </li>
        </ul>
    </div>
</nav>
