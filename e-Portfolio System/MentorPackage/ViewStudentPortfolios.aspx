﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MentorPackage/MentorTemplate.Master" AutoEventWireup="true" CodeBehind="ViewStudentPortfolios.aspx.cs" Inherits="e_Portfolio_System.MentorPackage.ViewStudentPortfolios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .normalTitle {
            font-weight: bold;
            font-size: 1.2em;
        }

        .ddlTitle {
            margin-left: 110px;
            margin-top: -15px;
            font-weight: 400;
        }

        .ddl {
            margin-left: 10px;
            height: 30px;
            border-radius: 7px;
            padding-left: 5px;
        }

        .ddl2 {
            margin-top: 10px;
            margin-left: 70px;
            height: 30px;
            border-radius: 7px;
            padding-left: 5px;
        }

        .rfv {
            margin-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-all">
    <p class="title">
        STUDENT PORTFOLIOS</p>
    <p>
        View Student Portfolios:
        <asp:Label ID="lblMessage" runat="server" style="font-family: 'Segoe UI'; font-weight: 400; font-style: italic; font-size: medium;"></asp:Label>
    </p>
    <p style="font-weight: 400;">
       Click on a student to view their portfolio, or search for a student here.
    </p>
    <p class="normalTitle">SEARCH STUDENT</p>
    <p class="ddlTitle">
        filter by:
        <asp:DropDownList ID="ddlSkills" runat="server" OnSelectedIndexChanged="ddlSkills_SelectedIndexChanged" Width="300px" AutoPostBack="True" CssClass="ddl">
        </asp:DropDownList>
        <asp:RequiredFieldValidator ID="rfvSkill1" runat="server" ControlToValidate="ddlSkills" Display="Dynamic" ErrorMessage="Please select a skill to filter for students." CssClass="rfv"></asp:RequiredFieldValidator>
        <br />
        <asp:DropDownList ID="ddlSkills2" runat="server" OnSelectedIndexChanged="ddlSkills2_SelectedIndexChanged" Width="300px" AutoPostBack="True" CssClass="ddl2" Enabled="False">
        </asp:DropDownList>
        <br />
    </p>
    <p class="normalTitle">
        <asp:GridView ID="gvStudent" runat="server" AutoGenerateColumns="False" CellPadding="3" GridLines="Horizontal" Width="100%" AllowPaging="True" OnPageIndexChanging="gvStudent_PageIndexChanging" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px">
            <AlternatingRowStyle BackColor="#FDFDFD" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="StudentID" DataNavigateUrlFormatString="ViewIndividualPortfolio.aspx?studentid={0}" DataTextField="StudentID" HeaderText="Student ID" />
                <asp:BoundField DataField="Name" HeaderText="Student Name" />
                <asp:ImageField DataImageUrlField="Photo" DataImageUrlFormatString="~/Images/{0}" HeaderText="Photo">
                    <ControlStyle Height="70px"/>
                </asp:ImageField>
                <asp:BoundField DataField="Course" HeaderText="Course" />
                <asp:TemplateField HeaderText="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%# displayYorN(Convert.ToString(Eval("Status"))) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
            <SortedAscendingCellStyle BackColor="#F4F4FD" />
            <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
            <SortedDescendingCellStyle BackColor="#D8D8F0" />
            <SortedDescendingHeaderStyle BackColor="#3E3277" />
        </asp:GridView>
        <br />
    </p>
    </div>
</asp:Content>
