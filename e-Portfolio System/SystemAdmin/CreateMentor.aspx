﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin/SystemAdministrator.Master" AutoEventWireup="true" CodeBehind="CreateMentor.aspx.cs" Inherits="e_Portfolio_System.CreateMentor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
        width: 350px;
    }
        .auto-style3 {
        font-family: "Segoe UI";
        color: #800080;
        font-style: italic;
        font-weight: bold;
        text-align: center;
    }
    .auto-style4 {
        width: 350px;
        text-align: right;
    }
        .auto-style5 {
            width: 350px;
            text-align: right;
            height: 46px;
        }
        .auto-style6 {
            height: 46px;
        }
        .auto-style7 {
            width: 350px;
            text-align: right;
            height: 47px;
        }
        .auto-style8 {
            height: 47px;
        }


        /*Textbox Css*/
                .textboxCss:first-child {
            margin-top: -5px;
        }

        .textboxCss {
            height: 45px;
            font-size: 1em;
            margin-top: 10px;
            border: none;
            padding: 10px 0;
            padding-bottom: 0px;
            border-bottom: solid 1px #6f1e7f;
            transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 98%, #6f1e7f 4%);
            background-position: -320px 0;
            background-size: 310px 100%;
            background-repeat: no-repeat;
            color: #333;
            font-weight: 500;
        }


        .textboxCss:focus {
            box-shadow: none;
            outline: none;
            background-position: 0 0;
            font-weight: 500;
            border-bottom: solid 2px #6f1e7f;
        }       

         .textboxCss:-webkit-autofill {
            -webkit-box-shadow: 0 0 0 30px white inset;
        }

         .buttonCss{
            background-color: #823791;
            color: #fff;
            border: none;
            font-weight: 500;
            width: 140px;
            height: 30px;
            border-radius : 5px;
         }

         .buttonCss:hover{
             background-color:#631572
         }



    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style3" colspan="2">Create Mentor Records</td>
        </tr>
        <tr>
            <td class="auto-style3" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4"><strong>Mentor ID :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblMentorID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style5"><strong>Name :</strong></td>
            <td class="auto-style6">
                &nbsp;
                <asp:TextBox ID="txtName" runat="server" CssClass="textboxCss" BorderColor="Gray"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="txtName" ErrorMessage="Name cannot be left blank" ForeColor="#CC3300" Display="Dynamic">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style7"><strong>Email</strong> <strong>Address :</strong></td>
            <td class="auto-style8">
                &nbsp;
                <asp:TextBox ID="txtEmail" runat="server" CssClass="textboxCss" BorderColor="Gray"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email Address cannot be left blank" Display="Dynamic" ForeColor="#CC3300">*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email Address has already been used" ForeColor="#CC3300" OnServerValidate="cvEmail_ServerValidate">*</asp:CustomValidator>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter a valid email" ForeColor="#CC3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                <br />
            </td>
        </tr>
        <tr>
            <td class="auto-style4"><strong>Password :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="LblPassword" runat="server" Text="p@55Mentor"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td>
                <br />
                <asp:Label ID="lblMessage" runat="server" ForeColor="#CC3300"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="#CC3300" />
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" class="buttonCss" />
                &nbsp;
                <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" class="buttonCss" CausesValidation="False" />
            </td>
        </tr>
    </table>
</asp:Content>
