﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System
{
    public partial class CreateStudent : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //StudentIDcount();

            if (Page.IsPostBack == false)
            {
                ddlCourse.Items.Add("IT");
                ddlCourse.Items.Add("FI");
                ddlCourse.Items.Add("ISF");
                ddlCourse.Items.Add("A3DA");

                fillddlmentor();

            }
        }



        private void fillddlmentor()
        {
            //Read Connection string ""Student_EPortfolio" from web.config file
            string StrConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //Instantate a SQLconnection object with the connection string read.
            SqlConnection conn = new SqlConnection(StrConn);

            //instantiate a sqlcommand object, supply it with the sql statement 
            //select that operates against the daabase, and the connection object
            //used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM Mentor ORDER BY Name ASC", conn);

            //instantiate a DataAdapter object and pass the sqlcommand object created as parameter
            SqlDataAdapter daMentor = new SqlDataAdapter(cmd);

            //create a DataSet object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operations made
            conn.Open();

            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            //Dataset "result" will store the results of the SELECT operation.
            daMentor.Fill(result, "MentorDetails");

            //A connection should always be closed, whether error occurs or not.
            conn.Close();

            ddlMentor.DataSource = result.Tables["MentorDetails"];
            ddlMentor.DataTextField = "Name";
            ddlMentor.DataValueField = "MentorID";
            ddlMentor.DataBind();

            ddlMentor.Items.Insert(0, "--Select--");


        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //create a new object from the staff class
                Student objStudent = new Student();

                objStudent.name = tbName.Text;
                objStudent.emailAddr = tbEmail.Text;
                objStudent.course = ddlCourse.SelectedValue;

                if (ddlMentor.SelectedIndex != 0)
                    objStudent.mentor = ddlMentor.SelectedValue;

                objStudent.password = lblPassword.Text;
                //call the add method to insert the staff record to database
                int errorCode = objStudent.add();


                if (errorCode == -2)
                {
                    lblMessage.Text = "Unable to create student record ";
                }

                else
                {
                    lblMessage.Text = "Student record created successfully";
                    lblStudentID.Text = errorCode.ToString();
                }

            }


        }

        protected void cvEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid)
            {
                Student objStudent = new Student();

                if (objStudent.isEmailExist(tbEmail.Text) == true)
                    args.IsValid = false; //raise error
                else
                    args.IsValid = true; //no error
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            tbName.Text = "";
            tbEmail.Text = "";
            lblStudentID.Text = "";
            lblMessage.Text = "";
            ddlCourse.SelectedIndex = 0;
            ddlMentor.SelectedIndex = 0;
        }
    }
}