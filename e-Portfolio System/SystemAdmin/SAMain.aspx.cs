﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System
{
    public partial class Main : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void ibMentor_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/SystemAdmin/CreateMentor.aspx");
        }

        protected void ibStudent_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/SystemAdmin/CreateStudent.aspx");
        }

        protected void ibSkillSet_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/SystemAdmin/CreateSkill.aspx");
        }

        protected void ibPhotograph_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/SystemAdmin/UploadPhotograph.aspx");
        }

        protected void ibApprove_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/SystemAdmin/ApproveRequest.aspx");
        }
    }
}