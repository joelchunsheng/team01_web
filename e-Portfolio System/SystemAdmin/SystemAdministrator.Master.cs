﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System
{
    public partial class SystemAdministrator : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoginID"] != null)
            {
                lblLoginID.Text = (string)Session["LoginID"];
            }
            else
            {
                Response.Redirect("Login.aspx");
            }

        }
    }
}