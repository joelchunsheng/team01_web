﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System
{
    public partial class ApproveRequest1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblRequestID.Text = Request.QueryString["ViewingRequestID"];
            lblParentID.Text = Request.QueryString["ParentID"];
            lblStudentName.Text = Request.QueryString["StudentName"];
            lblStudentID.Text = Request.QueryString["StudentID"];

        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                Student objstudent = new Student();

                objstudent.requestId = lblRequestID.Text;
                objstudent.studentId = Convert.ToInt32(txtStudentID.Text);

                objstudent.Accept();

                lblStudentID.Text = txtStudentID.Text;
                txtStudentID.Text = "";
                lblMessage.Text = "Request has been accepted!";
                btnApprove.Enabled = false;
                txtStudentID.Enabled = false;
            }
 

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ApproveRequest.aspx");
        }

        protected void CVstudentID_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid)
            {
                Student objStudent = new Student();

                if (objStudent.isIdExist(Convert.ToInt32(txtStudentID.Text)) == true)
                    args.IsValid = true; // no error 
                else
                    args.IsValid = false; // have error
            }
        }

        protected void CVstudentID2_ServerValidate(object source, ServerValidateEventArgs args)
        {

            Student objStudentName = new Student();

            objStudentName.studentId = Convert.ToInt32(txtStudentID.Text);
            string errorCode = objStudentName.getStudentName();


            if (errorCode == lblStudentName.Text)
                args.IsValid = true; // no error 
            else
                args.IsValid = false;


        }
    }
}