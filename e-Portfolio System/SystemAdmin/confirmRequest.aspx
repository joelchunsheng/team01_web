﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin/SystemAdministrator.Master" AutoEventWireup="true" CodeBehind="confirmRequest.aspx.cs" Inherits="e_Portfolio_System.ApproveRequest1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 500px;
        }

                 .buttonCss{
            background-color: #823791;
            color: #fff;
            border: none;
            font-weight: 500;
            width: 140px;
            height: 30px;
            border-radius : 5px;
         }

         .buttonCss:hover{
             background-color:#631572
         }

         .buttonCss:disabled{
            background-color: #823791;
            color: #fff;
            border: none;
            font-weight: 500;
            width: 140px;
            height: 30px;
            border-radius : 5px;
         }

                 .textboxCss:first-child {
            margin-top: -5px;
        }

        .textboxCss {
            height: 45px;
            font-size: 1em;
            margin-top: 10px;
            border: none;
            padding: 10px 0;
            padding-bottom: 0px;
            border-bottom: solid 1px #6f1e7f;
            transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 98%, #6f1e7f 4%);
            background-position: -320px 0;
            background-size: 310px 100%;
            background-repeat: no-repeat;
            color: #333;
            font-weight: 500;
        }


        .textboxCss:focus {
            box-shadow: none;
            outline: none;
            background-position: 0 0;
            font-weight: 500;
            border-bottom: solid 2px #6f1e7f;
        }       

         .textboxCss:-webkit-autofill {
            -webkit-box-shadow: 0 0 0 30px white inset;
        }
        .auto-style2 {
            width: 500px;
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>Viewing RequestID :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblRequestID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>ParentID :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblParentID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>Student Name :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblStudentName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>Student ID :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblStudentID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>Enter Student ID :</strong></td>
            <td>
                <asp:TextBox ID="txtStudentID" runat="server" CssClass="textboxCss"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtStudentID" ErrorMessage="StudentID cannot be left blank" ForeColor="#CC3300">*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="CVstudentID" runat="server" ControlToValidate="txtStudentID" Display="Dynamic" ErrorMessage="StudentID does not exist" ForeColor="#CC3300" OnServerValidate="CVstudentID_ServerValidate">*</asp:CustomValidator>
                <asp:CustomValidator ID="CVstudentID2" runat="server" ControlToValidate="txtStudentID" Display="Dynamic" ErrorMessage="Incorrect Student ID" ForeColor="#CC3300" OnServerValidate="CVstudentID2_ServerValidate">*</asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                <asp:Label ID="lblMessage" runat="server" ForeColor="#CC3300"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="#CC3300" />
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                <asp:Button ID="btnApprove" runat="server" OnClick="btnApprove_Click" Text="Approve" Width="140px" class="buttonCss"/>
&nbsp;<asp:Button ID="btnCancel" runat="server" Text="Back" Width="140px" OnClick="btnCancel_Click" class="buttonCss" CausesValidation="False"/>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
</asp:Content>
