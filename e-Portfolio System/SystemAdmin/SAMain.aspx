﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin/SystemAdministrator.Master" AutoEventWireup="true" CodeBehind="SAMain.aspx.cs" Inherits="e_Portfolio_System.Main" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
    .auto-style1 {
        padding-left: 30px;
        padding-right: 20px;
        padding-top: 40px;
    }
    .auto-style2 {
        color: #800000;
        font-style: italic;
        font-weight: bold;
    }
    .auto-style3 {
        text-align: center;
        height: 258px;
    }
    .auto-style4 {
        color: #800000;
        font-style: italic;
        font-weight: bold;
        padding-left: 25px;
        padding-right: 20px;
    }
    .auto-style5 {
        color: #800080;
        font-style: italic;
        font-weight: bold;
    }
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
    <tr>
        <td class="auto-style2">&nbsp;</td>
    </tr>
    <tr>
        <td class="auto-style5">Would you like to ...</td>
    </tr>
    <tr>
        <td class="auto-style3">
            <asp:ImageButton ID="ibMentor" runat="server" CssClass="auto-style1" Height="180px" ImageUrl="~/SystemAdmin/Images/ElementryTeacherIcon.png" OnClick="ibMentor_Click" Width="180px" />
            <asp:ImageButton ID="ibStudent" runat="server" CssClass="auto-style1" Height="180px" ImageUrl="~/SystemAdmin/Images/student.png" OnClick="ibStudent_Click" Width="180px" />
            <asp:ImageButton ID="ibSkillSet" runat="server" CssClass="auto-style1" Height="180px" ImageUrl="~/SystemAdmin/Images/skill.png" OnClick="ibSkillSet_Click" Width="180px" />
            <asp:ImageButton ID="ibPhotograph" runat="server" CssClass="auto-style1" Height="180px" ImageUrl="~/SystemAdmin/Images/Upload.png" OnClick="ibPhotograph_Click" Width="180px" />
            <asp:ImageButton ID="ibApprove" runat="server" CssClass="auto-style1" Height="180px" ImageUrl="~/SystemAdmin/Images/Approve.png" OnClick="ibApprove_Click" Width="180px" />
            <br />
        </td>
    </tr>
    <tr>
        <td class="text-center">
            <asp:Label ID="Label1" runat="server" CssClass="auto-style4" Text="Add Mentor Records" Width="180px"></asp:Label>
            <asp:Label ID="Label2" runat="server" CssClass="auto-style4" Text="Add Student Records" Width="200px"></asp:Label>
            <asp:Label ID="Label3" runat="server" CssClass="auto-style4" Text="Add Skill Set"></asp:Label>
            <asp:Label ID="Label4" runat="server" CssClass="auto-style4" Text="Upload Student Photograph" Width="200px"></asp:Label>
            <asp:Label ID="Label5" runat="server" CssClass="auto-style4" Text="Approve Request"></asp:Label>
        </td>
    </tr>
</table>
</asp:Content>
