﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin/SystemAdministrator.Master" AutoEventWireup="true" CodeBehind="ApproveRequest.aspx.cs" Inherits="e_Portfolio_System.ApproveRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
                 .buttonCss{
            background-color: #6f1e7f;
            color: #fff;
            border: none;
            font-weight: 500;
            width: 140px;
            height: 30px;
         }

        .rows:hover
        {
    	font-family: Arial;
        }



    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td>
                <strong>
                <asp:Label ID="lblPendingCount" runat="server" ForeColor="#CC3300" Text="Label"></asp:Label>
                </strong>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvResuest" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" Width="100%" RowStyle-CssClass="rows">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:BoundField DataField="ViewingRequestID" HeaderText="ViewingRequestID" />
                        <asp:BoundField DataField="ParentID" HeaderText="ParentID" />
                        <asp:BoundField DataField="StudentName" HeaderText="StudentName" />
                        <asp:BoundField DataField="StudentID" HeaderText="StudentID" />
                        <asp:HyperLinkField DataNavigateUrlFields="ViewingRequestID,ParentID,StudentName,StudentID,Status" DataNavigateUrlFormatString="confirmRequest.aspx?ViewingRequestID={0}&amp;ParentID={1}&amp;StudentName={2}&amp;StudentID={3}" Text="Approve" />
                        <asp:HyperLinkField DataNavigateUrlFields="ViewingRequestID,ParentID,StudentName,StudentID,Status" DataNavigateUrlFormatString="RejectRequest.aspx?ViewingRequestID={0}&amp;ParentID={1}&amp;StudentName={2}&amp;StudentID={3}" Text="Reject" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
