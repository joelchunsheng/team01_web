﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System
{
    public partial class ApproveRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                DisplayStudentList();
            }

        }

        private void DisplayStudentList()
        {
            //Read Connection string ""Student_EPortfolio" from web.config file
            string StrConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //Instantate a SQLconnection object with the connection string read.
            SqlConnection conn = new SqlConnection(StrConn);

            //instantiate a sqlcommand object, supply it with the sql statement 
            //select that operates against the daabase, and the connection object
            //used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM ViewingRequest WHERE Status = 'P'", conn);

            //instantiate a DataAdapter object and pass the sqlcommand object created as parameter
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //create a DataSet object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operations made
            conn.Open();

            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            //Dataset "result" will store the results of the SELECT operation.
            daStudent.Fill(result, "studentDetails");

            //A connection should always be closed, whether error occurs or not.
            conn.Close();

            gvResuest.DataSource = result.Tables["studentDetails"];

            gvResuest.DataBind();


            if (result.Tables["studentDetails"].Rows.Count > 0)
            {
                lblPendingCount.Text =(result.Tables["studentDetails"].Rows.Count).ToString() + " pending request";
            }
            else
            {
                lblPendingCount.Text = "There are no pending request";
            }
        }
    }
}