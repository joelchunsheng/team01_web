﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin/SystemAdministrator.Master" AutoEventWireup="true" CodeBehind="RejectRequest.aspx.cs" Inherits="e_Portfolio_System.RejectRequest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 500px;
        }

                 .buttonCss{
            background-color: #823791;
            color: #fff;
            border: none;
            font-weight: 500;
            width: 140px;
            height: 30px;
            border-radius : 5px;
         }

   .buttonCss:hover{
             background-color:#631572
         }
        .auto-style2 {
            width: 500px;
            text-align: right;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>Viewing RequestID :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblRequestID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>ParentID :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblParentID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>Student Name :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblStudentName" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style2"><strong>Student ID :</strong></td>
            <td>
                &nbsp;
                <asp:Label ID="lblStudentID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                <asp:Label ID="lblMessage" runat="server" ForeColor="#CC3300"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td>
                <asp:Button ID="btnReject" runat="server" OnClick="btnReject_Click" Text="Reject" class="buttonCss" Width="140px" />
&nbsp;<asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Back" class="buttonCss" CausesValidation="False" Width="140px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style1">
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
