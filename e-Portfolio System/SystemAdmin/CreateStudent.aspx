﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin/SystemAdministrator.Master" AutoEventWireup="true" CodeBehind="CreateStudent.aspx.cs" Inherits="e_Portfolio_System.CreateStudent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 350px;
        }
        .auto-style3 {
            font-family: "Segoe UI";
            color: #800080;
            font-style: italic;
            font-weight: bold;
            text-align: center;
            height: 38px;
        }
        .auto-style4 {
            width: 350px;
            text-align: right;
        }

         /*Textbox Css*/
                .textboxCss:first-child {
            margin-top: -5px;
        }

        .textboxCss {
            height: 45px;
            font-size: 1em;
            margin-top: 10px;
            border: none;
            padding: 10px 0;
            padding-bottom: 0px;
            border-bottom: solid 1px #6f1e7f;
            transition: all 0.3s cubic-bezier(0.64, 0.09, 0.08, 1);
            background: linear-gradient(to bottom, rgba(255, 255, 255, 0) 98%, #6f1e7f 4%);
            background-position: -320px 0;
            background-size: 310px 100%;
            background-repeat: no-repeat;
            color: #333;
            font-weight: 500;
        }


        .textboxCss:focus {
            box-shadow: none;
            outline: none;
            background-position: 0 0;
            font-weight: 500;
            border-bottom: solid 2px #6f1e7f;
        }       

         .textboxCss:-webkit-autofill {
            -webkit-box-shadow: 0 0 0 30px white inset;
        }
        .auto-style5 {
            width: 234px;
        }
        .auto-style6 {
            width: 124px;
            text-align: right;
        }

                 .buttonCss{
            background-color: #823791;
            color: #fff;
            border: none;
            font-weight: 500;
            width: 140px;
            height: 30px;
            border-radius : 5px;
         }

         .buttonCss:hover{
             background-color:#631572
         }

         .ddlClass{
             width: 100px;
         }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style3" colspan="4">Create Student Records</td>
        </tr>
        <tr>
            <td class="auto-style3" colspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4"><strong>Student ID :</strong></td>
            <td colspan="3">&nbsp;
                <asp:Label ID="lblStudentID" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4"><strong>Name :</strong></td>
            <td colspan="3">
                &nbsp;
                <asp:TextBox ID="tbName" runat="server" CssClass="textboxCss" BorderColor="Gray"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvName" runat="server" ControlToValidate="tbName" ErrorMessage="Name cannot be left blank" ForeColor="#CC3300">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style4"><strong>Email Address :</strong></td>
            <td colspan="3">
                &nbsp;
                <asp:TextBox ID="tbEmail" runat="server" CssClass="textboxCss" BorderColor="Gray"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ControlToValidate="tbEmail" Display="Dynamic" ErrorMessage="Email cannot be left blank" ForeColor="#CC3300">*</asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="tbEmail" ErrorMessage="Please enter a valid email" ForeColor="#CC3300" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator>
                <asp:CustomValidator ID="cvEmail" runat="server" ControlToValidate="tbEmail" ErrorMessage="Email address has already been used" ForeColor="#CC3300" OnServerValidate="cvEmail_ServerValidate">*</asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4"><strong>Course :</strong></td>
            <td class="auto-style5">
                &nbsp;
                <asp:DropDownList ID="ddlCourse" runat="server" CssClass="ddlClass">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvCourse" runat="server" ControlToValidate="ddlCourse" ErrorMessage="Please select a course" ForeColor="#CC3300">*</asp:RequiredFieldValidator>
            </td>
            <td class="auto-style6">
                Mentor :</td>
            <td>
                &nbsp;
                <asp:DropDownList ID="ddlMentor" runat="server" CssClass="ddlClass">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvMentor" runat="server" ControlToValidate="ddlMentor" Display="Dynamic" ErrorMessage="Please select a mentor" ForeColor="#CC3300">*</asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4"><strong>Password :</strong></td>
            <td colspan="3">&nbsp;
                <asp:Label ID="lblPassword" runat="server" Text="p@55Student"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td colspan="3">
                <br />
                <asp:Label ID="lblMessage" runat="server" ForeColor="#CC3300"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td colspan="3">
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="#CC3300" />
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td colspan="3">
                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" Width="140px" class="buttonCss" />
                &nbsp;
                <asp:Button ID="btnClear" runat="server" OnClick="btnClear_Click" Text="Clear" Width="140px" class="buttonCss" CausesValidation="False" />
            </td>
        </tr>
    </table>
</asp:Content>
