﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System
{
    public partial class CreateSkill : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cvSkillSet_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid)
            {
                SkillSet objSkill = new SkillSet();

                if (objSkill.isSkillExist(tbSkillSet.Text) == true)
                    args.IsValid = false; //raise error
                else
                    args.IsValid = true; //no error
            }
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                //create a new object from the staff class
                SkillSet objSkillSet = new SkillSet();

                objSkillSet.name = tbSkillSet.Text;

                //call the add method to insert the staff record to database
                int id = objSkillSet.add();

                lblMessage.Text = "Skill Set has been created!";
                tbSkillSet.Text = "";

            }
        }


    }
}