﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System
{
    public partial class CreateMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

       

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                // Create a new object from Mentor class
                Mentor objMentor = new Mentor();

                objMentor.name = txtName.Text;
                objMentor.email = txtEmail.Text;
                objMentor.password = LblPassword.Text;

                // Call the add method to insert the staff record to database
                int id = objMentor.add();

                lblMessage.Text = "Mentor record created successfully";
                lblMentorID.Text = id.ToString();
                
            }
        }

        protected void cvEmail_ServerValidate(object source, ServerValidateEventArgs args)
        {
            if (Page.IsValid)
            {
                Mentor objMentor = new Mentor();

                if (objMentor.isEmailExist(txtEmail.Text) == true)
                    args.IsValid = false; // raise error
                else
                    args.IsValid = true; // no error
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            txtName.Text = "";
            txtEmail.Text = "";
            lblMentorID.Text = "";
            lblMessage.Text = "";
        }
    }
}