﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="~/SystemAdmin/AdminMenu.ascx.cs" Inherits="e_Portfolio_System.Menu" %>
<style>
    nav {
        text-align: center;
        position: fixed;
        top: 0;
        font-size: 14px;
        transition: all 0.6s ease-out;
        z-index: 99;
        height: 90px;
    }

    nav a:first-child {
        margin-left: 30px;
    }

    #firstNavItem {
        margin-left: 30px;
    }

    #adminNavbar a {
        display: inline-block;
        padding: 25px 10px 0 20px;
        text-transform: uppercase;
        text-decoration: none;
        color: #7c7c7c;
        font-weight: 500;
        border-left: 1.5px solid #7c7c7c;
        cursor: pointer;
    }

    #adminNavbar a:hover {
        color: #6f1e7f;
        border-left: 1.5px solid #6f1e7f;
    }

    .btnLogOut {
        margin-right: 50px;
        border-radius: 0;
        padding: 25px 10px 0 20px;
        font-size: 14px;
        color: #7c7c7c;
        font-weight: 500;
        border-left: 1.5px solid #7c7c7c;
    }

    .btnLogOut:hover {
        color: #6f1e7f;
        border-left: 1.5px solid #6f1e7f;
        text-decoration: none;
    }
</style>
<nav class="navbar navbar-expand-md navbar-light">

	  <!-- Brand -->
	 <a class="navbar-brand" href="SAMain.aspx">
         <img src="../Images/Assets/ict_logo.png" alt="ICT Logo" width="55"/>
	 </a>

	  <!-- Toggler/collapsibe Button -->
	  <button class="navbar-toggler" type="button" 
          data-toggle="collapse" data-target="#adminNavbar">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <!-- Navbar links -->
	  <div class="collapse navbar-collapse" id="adminNavbar">
	    <!-- Links that are aligned to the left, mr-auto: right margin auto-adjusted -->
		<ul class="navbar-nav mr-auto">
		  <li class="nav-item">
		    <a class="nav-link" href="CreateMentor.aspx" >Create Mentor Records</a>
		  </li>

          <li class="nav-item">
		    <a class="nav-link" href="CreateStudent.aspx">Create Student Records</a>
		  </li>

          <li class="nav-item">
		    <a class="nav-link" href="CreateSkill.aspx">Create Skill Set</a>
		  </li>

            <li class="nav-item">
		    <a class="nav-link" href="UploadPhotograph.aspx">Upload Student Photograph</a></li>

            <li class="nav-item">
		    <a class="nav-link" href="ApproveRequest.aspx">Approve Parents Request</a>
		  </li>

          
         </ul>

		<!-- Links that are aligned to the right, ml-auto: left margin auto-adjusted -->
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
                <asp:Button ID="btnLogOut" runat="server" Text="Log Out" CssClass="btn btn-link nav-link" OnClick="btnLogOut_Click" CausesValidation="False" />
			</li>
		</ul>
	  </div>
	</nav> 