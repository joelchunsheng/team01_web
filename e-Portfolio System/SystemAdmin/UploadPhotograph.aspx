﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin/SystemAdministrator.Master" AutoEventWireup="true" CodeBehind="UploadPhotograph.aspx.cs" Inherits="e_Portfolio_System.UploadPhoto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 350px;
        }
        .auto-style3 {
            text-align: left;
            font-family: "Segoe UI";
            font-style: italic;
            font-weight: bold;
            color: #800080;
        }
        .auto-style4 {
            width: 350px;
            height: 40px;
        }
        .auto-style5 {
            height: 40px;
        }
        .auto-style6 {
            text-align: center;
            font-family: "Segoe UI";
            font-style: italic;
            font-weight: bold;
            color: #800080;
        }
        .auto-style7 {
            text-align: center;
            width: 350px;
        }
        .auto-style8 {
            width: 350px;
            text-align: right;
            height: 46px;
        }
        .auto-style9 {
            height: 46px;
        }

         .buttonCss{
            background-color: #823791;
            color: #fff;
            border: none;
            font-weight: 500;
            width: 140px;
            height: 30px;
            border-radius : 5px;
            
         }

         .buttonCss:hover{
             background-color:#631572
         }

         
         .ddlClass{
             width: 100px;
         }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td colspan="2" class="auto-style6" >Upload Students Photograph</td>
        </tr>
        <tr>
            <td class="auto-style7" >&nbsp;</td>
            <td class="auto-style3" >&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style8"><strong>Select Student ID :</strong></td>
            <td class="auto-style9">
                &nbsp;
                <asp:DropDownList ID="ddlstudentid" runat="server" placeholder="Select StudentID" CssClass="ddlClass">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:Image ID="imgPhoto" runat="server" Height="100px" />
            </td>
        </tr>
        <tr>
            <td class="auto-style4"></td>
            <td class="auto-style5">
                <asp:FileUpload ID="UPphoto" runat="server" ForeColor="Black" />
                <asp:RequiredFieldValidator ID="rfvUploadPic" runat="server" ControlToValidate="UPphoto" ErrorMessage="Please select a picture" ForeColor="#CC3300">*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvUploadImage" runat="server" ControlToValidate="UPphoto" ErrorMessage="Image file type is not supported" ForeColor="#CC3300" OnServerValidate="cvUploadImage_ServerValidate">*</asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:Label ID="lbltest" runat="server" ForeColor="#CC3300"></asp:Label>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="#CC3300" />
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" Width="140px" class="buttonCss" />
                <asp:Label ID="lblMessage" runat="server" ForeColor="#CC3300"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
