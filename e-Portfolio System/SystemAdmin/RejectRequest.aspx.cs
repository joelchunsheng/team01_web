﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System
{
    public partial class RejectRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblRequestID.Text = Request.QueryString["ViewingRequestID"];
            lblParentID.Text = Request.QueryString["ParentID"];
            lblStudentName.Text = Request.QueryString["StudentName"];
            lblStudentID.Text = Request.QueryString["StudentID"];

        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            Student objstudent = new Student();

            objstudent.requestId = lblRequestID.Text;
            objstudent.Reject();
            lblMessage.Text = "Request has been rejected!";
            btnReject.Enabled = false;

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("ApproveRequest.aspx");
        }
    }
}