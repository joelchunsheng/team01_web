﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System
{
    public partial class UploadPhoto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                fillddlstudent();
            }

        }

      

        private void fillddlstudent()
        {
            //Read Connection string ""Student_EPortfolio" from web.config file
            string StrConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //Instantate a SQLconnection object with the connection string read.
            SqlConnection conn = new SqlConnection(StrConn);

            //instantiate a sqlcommand object, supply it with the sql statement 
            //select that operates against the daabase, and the connection object
            //used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT * FROM Student WHERE Photo IS NULL", conn);

            //instantiate a DataAdapter object and pass the sqlcommand object created as parameter
            SqlDataAdapter dastudent = new SqlDataAdapter(cmd);

            //create a DataSet object to contain the records retrieved from database
            DataSet result = new DataSet();

            //A connection must be opened before any operations made
            conn.Open();

            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet.
            //Dataset "result" will store the results of the SELECT operation.
            dastudent.Fill(result, "studentDetails");

            //A connection should always be closed, whether error occurs or not.
            conn.Close();

            if (result.Tables["studentDetails"].Rows.Count > 0)
            {
                ddlstudentid.DataSource = result.Tables["studentDetails"];
                ddlstudentid.DataTextField = "StudentID";
                ddlstudentid.DataBind();
            }

            else
            {
                ddlstudentid.Enabled = false;
                btnUpload.Enabled = false;
                UPphoto.Enabled = false;
                lblMessage.Text = "There are no more outstanding students without image";
  
            }


        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                string uploadedFile = "";
                if (UPphoto.HasFile == true)
                {
                    string savePath;

                    string fileExt = Path.GetExtension(UPphoto.FileName);
                    uploadedFile = ddlstudentid.SelectedValue + fileExt;
                    savePath = MapPath("~/Images/" + uploadedFile);

                    try
                    {
                        UPphoto.SaveAs(savePath);
                        lblMessage.Text = "File Uploaded";
                        imgPhoto.ImageUrl = "~/Images/" + uploadedFile;


                        Student objStudent = new Student();

                        //read staff ID from query string
                        objStudent.studentId = Convert.ToInt32(ddlstudentid.SelectedValue);
                        objStudent.photo = uploadedFile;
                        objStudent.update();
                        fillddlstudent();

                    }
                    catch
                    {
                        lblMessage.Text = "File uploading fail";
                    }
                }


            }

        }

        protected void cvUploadImage_ServerValidate(object source, ServerValidateEventArgs args)
        {

            string fileCheck = Path.GetExtension(UPphoto.FileName);


            if (fileCheck == ".jpg" )
                args.IsValid = true; // no error 

            else if (fileCheck == ".png")
                args.IsValid = true; // no error 
            else
                args.IsValid = false;
        }
    }
}