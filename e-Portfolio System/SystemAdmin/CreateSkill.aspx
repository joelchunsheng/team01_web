﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SystemAdmin/SystemAdministrator.Master" AutoEventWireup="true" CodeBehind="CreateSkill.aspx.cs" Inherits="e_Portfolio_System.CreateSkill" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style2 {
            width: 350px;
        }
        .auto-style3 {
            font-family: "Segoe UI";
            color: #800080;
            text-transform: uppercase;
            font-style: italic;
            font-weight: bold;
        }
        .auto-style4 {
            width: 350px;
            text-align: right;
        }
        .auto-style5 {
            font-family: "Segoe UI";
            color: #800080;
            font-style: italic;
            font-weight: bold;
            text-align: center;
        }


        /*Skill Set Textbox CSS*/
        .skillsetDiv {
            position: relative;
            margin-top: 50px;
            display: inline-block;
            width: 500px;
            height: 40px;
            box-sizing: border-box;
            outline: none;
            border: 1px solid lightgray;
            border-radius: 3px;
            transition: all 0.1s ease-out;
        }
  
      .TextBoxClass{
        display: inline-block;
        width: 500px;
        height: 40px;
        box-sizing: border-box;
        outline: none;
        border: 1px solid lightgray;
        border-radius: 3px;
        padding: 10px 10px 10px 100px;
        transition: all 0.1s ease-out;
      }
  
      .TextBoxClass + .LabelClass{
        position: absolute;
        top: 0;
        left: 0;
        bottom: 0;
        height: 40px;
        line-height: 40px;
        color: white;
        border-radius: 3px 0 0 3px;
        padding: 0 20px;
        background: #6e418c;
        transform: translateZ(0) translateX(0);
        transition: all 0.3s ease-in;
        transition-delay: 0.2s;
      }
  
      .TextBoxClass:focus + .LabelClass{
        transform: translateY(-120%) translateX(0%);
        border-radius: 3px;
        transition: all 0.1s ease-out;
      }
  
      .TextBoxClass:focus{
        padding: 10px;
        transition: all 0.3s ease-out;
        transition-delay: 0.2s;
      }

               .buttonCss{
            background-color: #823791;
            color: #fff;
            border: none;
            font-weight: 500;
            width: 140px;
            height: 30px;
            border-radius : 5px;
         }

                 .buttonCss:hover{
             background-color:#631572
         }

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style5" colspan="2">Create Skill Set</td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td class="auto-style3">&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td>&nbsp;

                <div class="skillsetDiv">
                    <asp:TextBox class="TextBoxClass" ID="tbSkillSet" runat="server" placeholder="Enter Skill Set"></asp:TextBox>
                    <asp:Label class="LabelClass" ID="lblSkill" runat="server" Text="Skill set :"></asp:Label>
                </div>



                <asp:RequiredFieldValidator ID="rfvSkillSet" runat="server" ControlToValidate="tbSkillSet" Display="Dynamic" ErrorMessage="Skill set cannot be left blank" ForeColor="#CC3300">*</asp:RequiredFieldValidator>
                <asp:CustomValidator ID="cvSkillSet" runat="server" ControlToValidate="tbSkillSet" Display="Dynamic" ErrorMessage="Skill Set already exist" ForeColor="#CC3300" OnServerValidate="cvSkillSet_ServerValidate">*</asp:CustomValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td>
                <br />
&nbsp;
                <asp:Label ID="lblMessage" runat="server" ForeColor="#CC3300"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style4">&nbsp;</td>
            <td>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="#CC3300" />
            </td>
        </tr>
        <tr>
            <td class="auto-style2">&nbsp;</td>
            <td>
                <asp:Button ID="btnCreate" runat="server" Text="Create" OnClick="btnCreate_Click" Width="140px" class="buttonCss" />
&nbsp;</td>
        </tr>
    </table>
</asp:Content>
