﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="ContactMentor.aspx.cs" Inherits="e_Portfolio_System.ParentPackage.ContactMentor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            font-weight: normal;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="title">
            e-Messages Board</p>
    <p>
        Click on the mentor below to get in touch.</p>
    <p class="auto-style1">
        <em>Note: You may only contact your child&#39;s mentor.</em></p>
    <p class="auto-style2">
        <asp:GridView ID="gvMessages" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="5" GridLines="Horizontal" Width="100%" AllowPaging="True" >
            <AlternatingRowStyle BackColor="#F7F7F7" />
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="MentorID" DataNavigateUrlFormatString="ViewMentorMessage.aspx?mentorid={0}" DataTextField="MentorID" HeaderText="Mentor ID" />
                <asp:BoundField DataField="Mentor Name" HeaderText="Mentor's Name" />
                <asp:BoundField DataField="Student Name" HeaderText="Child's Name" />
            </Columns>
            <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
            <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
            <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
            <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
            <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
            <SortedAscendingCellStyle BackColor="#F4F4FD" />
            <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
            <SortedDescendingCellStyle BackColor="#D8D8F0" />
            <SortedDescendingHeaderStyle BackColor="#3E3277" />
        </asp:GridView>
    </p>
</asp:Content>
