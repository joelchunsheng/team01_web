﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="ViewMentorMessage.aspx.cs" Inherits="e_Portfolio_System.ViewMentorMessage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 923px;
            height: 125px;
        }
        .auto-style2 {
            color: #660066;
        }
        .auto-style3 {
            color: #660066;
            font-size: medium;
        }
        .auto-style4 {
            color: #660066;
            font-size: large;
        }
        .auto-style5 {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p class="title">
        CONTACT MENTOR</p>
    <div id="messages">
        <p class="auto-style3">
            &nbsp;<asp:Label ID="lblTitle" runat="server" CssClass="auto-style2" style="font-size: x-large"></asp:Label>
        </p>
        <p class="auto-style2">
            <asp:Label ID="lblNoMessage" runat="server"></asp:Label>
        </p>
        <p>
            Posted by <asp:Label ID="lblName" runat="server"></asp:Label>
            at 
            <asp:Label ID="lblDate" runat="server"></asp:Label>
            .</p>
        <p>
            Message:<br />
            <asp:Label ID="lblText" runat="server" style="font-weight:400;"></asp:Label>
        </p>
        <hr />
    </div>
    <div runat="server" id="appendReply">
        <span class="auto-style4"><strong>Title:</strong></span>
        <asp:TextBox ID="txtTitle" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle" CssClass="auto-style5" Display="Dynamic" ErrorMessage="*Required"></asp:RequiredFieldValidator>
    </div>
    <p id="replyTable">
        <br />Message:<br />
        <textarea id="taMessage" class="auto-style1" name="S1" runat="server"></textarea><asp:RequiredFieldValidator ID="rfvMessage" runat="server" ControlToValidate="taMessage" CssClass="auto-style5" Display="Dynamic" ErrorMessage="*Required"></asp:RequiredFieldValidator>
        <br />
    </p>
    <p>
        <asp:Button ID="btnMessage" runat="server" Text="Post Message" Width="135px" OnClick="btnMessage_Click"  />
    </p>
</asp:Content>
