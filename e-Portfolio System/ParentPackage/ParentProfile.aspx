﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="ParentProfile.aspx.cs" Inherits="e_Portfolio_System.ParentPackage.ParentProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            height: 108px;
            width: 442px;
        }
        .auto-style2 {
            height: 108px;
            width: 298px;
            color: #660066;
        }
        .auto-style3 {
            font-size: x-large;
        }
        .auto-style4 {
            font-size: xx-large;
        }
        .auto-style5 {
            color: #660066;
        }
        .auto-style6 {
            margin-left: 40px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="profile-header">
        <table class="w-100">
            <tr>
                <td class="auto-style2">
                    <span class="auto-style4"><strong>&nbsp;&nbsp;
                    PARENT PROFILE</strong></span><strong><br class="auto-style3" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="lblName" runat="server" CssClass="auto-style3"></asp:Label>
                    <br class="auto-style3" />
                    </strong><span class="auto-style3"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    Parent/Guardian</strong></span></td>
                <td class="auto-style1">
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td class="auto-style5">
                    <aside id="portfolio-summary">
                        <div class="auto-style6">

                            <span class="auto-style3"><strong>EMAIL ADDRESS:</strong></span><strong><br class="auto-style3" />
                    <asp:Label ID="lblParentEmail" runat="server" CssClass="auto-style3"></asp:Label>
                            <br class="auto-style3" />

                            </strong>

                        </div>
                    </aside>
                </td>
                <td class="profile-label">
                    <table class="w-100">
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:GridView ID="gvChild" runat="server" AllowPaging="True" AutoGenerateColumns="False" PageSize="4">
                                    <Columns>
                                        <asp:HyperLinkField DataNavigateUrlFields="StudentID" DataNavigateUrlFormatString="ViewChildPortfolio.aspx?studentid={0}" DataTextField="StudentID" HeaderText="STUDENT ID" />
                                        <asp:BoundField DataField="name" HeaderText="NAME" />
                                        <asp:BoundField DataField="course" HeaderText="COURSE" />
                                        <asp:BoundField DataField="ViewingStatus" HeaderText="VIEWING STATUS" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            </table>
        
    </div>
    
</asp:Content>