﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="ParentMain.aspx.cs" Inherits="e_Portfolio_System.ParentPackage.ParentMain" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .imageButton{
            width: 1100px;
            text-align: center;
            margin: 0 auto;
            margin-top: 40px;
        }

        .imageLabel{
            text-align:center;
            font-weight: 700; 
            color: #660066;
        }
       
        .label{
            display:inline-block; 
            margin: 0 35px; 
            max-width: 130px;
        }


    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p style="margin-left: 40px; font-size: 25px;">
        Welcome,
        <asp:Label ID="parentLabel" runat="server"></asp:Label>
        !</p>
    <div class="imageButton">
        <asp:ImageButton ID="view" Width="200px" style="padding: 40px" runat="server" ImageUrl="~\Images\Assets\Parent\viewport.png" PostBackUrl="~\ParentPackage\ViewingRequestDetails.aspx" />
        <asp:ImageButton ID="contact" Width="200px" style="padding: 40px"  runat="server" ImageUrl="~\Images\Assets\Parent\contactmentor.png" PostBackUrl="~\ParentPackage\ContactMentor.aspx" />
        <asp:ImageButton ID="search" Width="200px" style="padding: 40px" runat="server" ImageUrl="~\Images\Assets\Parent\search.png" PostBackUrl="~\ParentPackage\StudentPortfolioList.aspx" />    
    </div>
    <div class="imageLabel">
        <div class="label">
            view student portfolio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <div class="label">
            message mentor
        </div>
        <div class="label">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            search for a student
        </div>
    </div>
</asp:Content>
