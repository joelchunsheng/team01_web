﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="AllConversations.aspx.cs" Inherits="e_Portfolio_System.ParentPackage.AllConversations" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" runat="server" contentplaceholderid="ContentPlaceHolder1">
    All Conversations</p><asp:GridView ID="gvConversation" runat="server" AutoGenerateColumns="False">

                         
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="MessageID" DataNavigateUrlFormatString="ConversationHistoryWithMentor.aspx?MessageID={0}" DataTextField="MessageID" HeaderText="MessageID" />
            <asp:BoundField DataField="Title" HeaderText="Title" />
            <asp:BoundField DataField="Mentor Name" HeaderText="Mentor Name" />
            <asp:BoundField DataField="Parent Name" HeaderText="Parent Name"/>
            <asp:BoundField DataField="DateTimePosted" HeaderText="Date Posted"/>
            <asp:BoundField DataField="Text" HeaderText="Text" SortExpression="Text" />
        </Columns>
        </asp:GridView> >
</asp:Content>

