﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace e_Portfolio_System.ParentPackage
{
    public partial class ViewStudentPortfolio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["studentid"] != null)
                {
                    // Create a new Student object
                    Student objStudent = new Student();

                    // Read Student ID from query string
                    objStudent.studentId = Convert.ToInt32(Request.QueryString["studentid"]);

                    // Load student information to controls
                    int errorCode = objStudent.getPortfolioDetails();
                    if (errorCode == 0)
                    {
                        imgChild.ImageUrl = "~/Images/" + objStudent.photo;
                        lblName.Text = objStudent.name;
                        lblDescription.Text = objStudent.description;
                        lblAchievements.Text = objStudent.achievement;
                        lnkLinkedIn.NavigateUrl = objStudent.externalLink;
                        lnkLinkedIn.Text = objStudent.externalLink;
                        lnkEmail.NavigateUrl = "mailto:" + objStudent.emailAddr;
                        lnkEmail.Text = objStudent.emailAddr;

                        if (objStudent.studentSkillList.Count() > 0)
                        {
                            lblSkills.Text = objStudent.studentSkillList[0].ToString();
                            if (objStudent.studentSkillList.Count() > 1)
                            {
                                for (int i = 1; i < objStudent.studentSkillList.Count; i++)
                                    lblSkills.Text += "; " + objStudent.studentSkillList[i].ToString();
                            }
                        }


                    }
                    else if (errorCode == -2)
                    {
                        lblErrorMessage.Enabled = true;
                        lblErrorMessage.Text = "Unable to retrieve portfolio for Student ID " +
                                           objStudent.studentId;
                        lblErrorMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }
            }
        }
    }
}
