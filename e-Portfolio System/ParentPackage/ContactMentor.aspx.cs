﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.ParentPackage
{
    public partial class ContactMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            displayContactMentorList();
        }
        public void displayContactMentorList()
        {
            string strConn = ConfigurationManager.ConnectionStrings
                                ["Student_EPortfolio"].ToString();

            //Instantiate a SqlConnection object with the Connection String read.
            SqlConnection conn = new SqlConnection(strConn);

            // Instantiate a SqlCommand object, supply it with an INSERT SQL statement 
            // which will return the auto-generated StaffID after insertion,
            // and the connection object for connecting to the database.

            SqlCommand cmd = new SqlCommand(
                "Select Mentor.MentorID,Mentor.Name as 'Mentor Name',Student.Name as 'Student Name' from Mentor inner join Student on Student.MentorID = Mentor.MentorID inner join  ViewingRequest on Student.StudentID = ViewingRequest.StudentID inner join Parent on ViewingRequest.ParentID = Parent.ParentID where ViewingRequest.Status='A' and Parent.ParentID=@ParentID", conn);

            //Define parameters
            
            cmd.Parameters.AddWithValue("@ParentID", Convert.ToInt32(Session["ParentID"]));
            SqlDataAdapter daContact = new SqlDataAdapter(cmd);
            DataSet results = new DataSet();

            //Connection to database must be opened before any ops are made
            conn.Open();
            daContact.Fill(results, "ContactList");
            conn.Close();
            gvMessages.DataSource = results.Tables["ContactList"];
            gvMessages.DataBind();
        }

      
    }
}