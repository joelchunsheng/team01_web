﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System
{
    public partial class ViewMentorMessage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                Message objMessage = new Message();

                DataSet result = new DataSet();

                objMessage.fromId = (int)Session["ParentId"];
                objMessage.toId = Convert.ToInt32(Request.QueryString["mentorid"]);

                int errorCode = objMessage.getMessageDetails(ref result);
                if (errorCode == 0)
                {
                    lblTitle.Text = objMessage.title;
                    lblName.Text = objMessage.parentName;
                    lblDate.Text = objMessage.dateTimePosted;
                    lblText.Text = objMessage.text;

                    
                    //toAppendReply();
                }



            }


        }

        protected void btnMessage_Click(object sender, EventArgs e)
        {
            int mentorid = Convert.ToInt32(Request.QueryString["mentorid"]);
            int parentid = (int)Session["parentid"];

            Message newMessage = new Message();

            newMessage.fromId = parentid;
            newMessage.toId = mentorid;
            newMessage.dateTimePosted = DateTime.Now.ToString();
            newMessage.title = txtTitle.Text;
            newMessage.text = taMessage.Value;

            
            int id = newMessage.add();

            




        }

        
    }
}
    
