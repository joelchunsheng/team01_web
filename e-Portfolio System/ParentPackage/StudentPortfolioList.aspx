﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="StudentPortfolioList.aspx.cs" Inherits="e_Portfolio_System.ParentPackage.StudentPortfolioList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 590px;
        }
        .auto-style2 {
            font-size: xx-small;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style1">STUDENT PORTOFOLIOS<br />
                <span class="auto-style2">NOTE: ONLY PARENTS/GUARDIANS OF STUDENTS MAY REQUEST TO VIEW FULL PORTFOLIO.</span></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="auto-style1">
                &nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td colspan="2" class="text-center">
                <asp:GridView ID="gvStudent" runat="server" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellPadding="3" CellSpacing="1" GridLines="None" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="gvStudent_PageIndexChanging" Width="692px">
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="StudentId" DataNavigateUrlFormatString="ViewStudentPortfolio.aspx?studentid={0}" DataTextField="StudentId" HeaderText="STUDENT ID" />
                        <asp:BoundField DataField="Name" HeaderText="NAME" />
                        <asp:BoundField DataField="Course" HeaderText="CLASS" />
                        <asp:HyperLinkField HeaderText="SEND REQUEST" NavigateUrl="ViewingRequestDetails.aspx" Text="Submit" />
                    </Columns>
                    <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
                    <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
                    <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#594B9C" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#33276A" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>