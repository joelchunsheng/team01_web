﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="ViewChildPortfolio.aspx.cs" Inherits="e_Portfolio_System.ParentPackage.ViewChildPortfolio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 324px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <table class="w-100">
        <tr>
            <td class="auto-style1" rowspan="4">
                <asp:Image ID="imgChild" runat="server" Height="170px" Width="170px" />
            </td>
            <td>
                <br />
                <h1><strong>
                    <asp:Label ID="lblName" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblErrorMessage" runat="server" Enabled="False"></asp:Label>
                    </strong></h1>
            </td>
        </tr>
        <tr>
            <td>
                <h2><strong>
                    <asp:Label ID="lblCourse" runat="server"></asp:Label>
                    </strong></h2>
            </td>
        </tr>
        <tr>
            <td>
                <h4>
                    <asp:HyperLink ID="lnkEmail" runat="server" Target="_blank">[lnkEmail]</asp:HyperLink>
                </h4>
            </td>
        </tr>
        <tr>
            <td>
                <h4>
                    <asp:HyperLink ID="lnkLinkedIn" runat="server" Target="_blank">[lnkLinkedIn]</asp:HyperLink>
                </h4>
            </td>
        </tr>
        <tr>
            <td class="auto-style1"><strong>DESCRIPTION:</strong></td>
            <td>
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1"><strong>SKILLS:</strong></td>
            <td>
                <asp:Label ID="lblSkills" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="auto-style1"><strong>ACHIEVEMENTS:</strong></td>
            <td>
                <asp:Label ID="lblAchievements" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="text-center" colspan="2">
                <strong>PROJECTS<br />
                </strong>
                <asp:Label ID="lblNoProject" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="text-center">
                <asp:GridView ID="gvProjectList" runat="server" AutoGenerateColumns="False">
                    <Columns>
                        <asp:BoundField DataField="Title" HeaderText="TITLE" />
                        <asp:ImageField DataImageUrlField="ProjectPoster" DataImageUrlFormatString="~/Images/Projects/{0}" HeaderText="POSTER">
                        </asp:ImageField>
                        <asp:BoundField DataField="Description" HeaderText="DESCRIPTION" />
                        <asp:HyperLinkField DataTextField="ProjectUrl" DataTextFormatString="{0}" HeaderText="PROJECT LINK" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    
</asp:Content>
