﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.ParentPackage
{
    public partial class AllConversations : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            displayAllConversations();
        }
        public void displayAllConversations()
        {
            
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);

            SqlCommand cmd = new SqlCommand("Select MessageID, Title, Mentor.Name as 'Mentor Name', Parent.ParentName as 'Parent Name', DateTimePosted,Text " +
                "from Message inner join Parent on Parent.ParentID = Message.FromID inner join Mentor on Mentor.MentorID = Message.ToID where ParentID = @ParentID ", conn);
            cmd.Parameters.AddWithValue("@ParentID",Session["ParentID"].ToString() );


            SqlDataAdapter daApproval = new SqlDataAdapter(cmd);

            DataSet data = new DataSet();

            conn.Open();

            daApproval.Fill(data, "AllConversations");
            conn.Close();

            gvConversation.DataSource = data.Tables["AllConversations"];

            gvConversation.DataBind();

           
        }
    }

}