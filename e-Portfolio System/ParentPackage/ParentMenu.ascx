﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ParentMenu.ascx.cs" Inherits="e_Portfolio_System.ParentPackage.ParentMenu" %>

<style>
    nav {
        text-align: center;
        position: fixed;
        top: 0;
        font-size: 14px;
        transition: all 0.6s ease-out;
        z-index: 99;
        height: 90px;
    }

    nav a:first-child {
        margin-left: 30px;
    }

    #firstNavItem {
        margin-left: 30px;
    }

    #parentNavbar a {
        display: inline-block;
        padding: 25px 10px 0 20px;
        text-transform: uppercase;
        text-decoration: none;
        color: #7c7c7c;
        font-weight: 500;
        border-left: 1.5px solid #7c7c7c;
        cursor: pointer;
    }

    #parentNavbar a:hover {
        color: #6f1e7f;
        border-left: 1.5px solid #6f1e7f;
    }

    .logOutButton {
        margin-right: 50px;
        border-radius: 0;
        padding: 25px 10px 0 20px;
        font-size: 14px;
        color: #7c7c7c;
        font-weight: 500;
        border-left: 1.5px solid #7c7c7c;
    }

    .logOutButton:hover {
        color: #6f1e7f;
        border-left: 1.5px solid #6f1e7f;
        text-decoration: none;
    }
</style>

<!-- A grey navbar that expands horizontally at medium device -->
<nav class="navbar navbar-expand-md navbar-light">
    
    <!-- The brand (or icon) of the navbar -->
    <a class="navbar-brand"  href="ParentMain.aspx">
        <img src="../Images/Assets/ict_logo.png" alt="ICT Logo" width="55"/>
    </a>
 
    <!-- Toggle/collapsible Button, also known as hamburger button -->
    <button class="navbar-toggler" type="button"
        data-toggle="collapse" data-target="#parentNavbar">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <!-- Links in the navbar, displayed as drop-down list when collapsed -->
    <div class="collapse navbar-collapse" id="parentNavbar">
        <!-- Links that are aligned to the left, mr-auto: right margin auto-adjusted -->
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="ParentProfile.aspx">PROFILE</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="StudentPortfolioList.aspx">STUDENTS' PORTFOLIOS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="ContactMentor.aspx">E-MESSAGES BOARD</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="AllConversations.aspx">CONVERSATIONS</a>
            </li>
        </ul>
 
        <!-- Links that are aligned to the right, ml-auto: left margin auto-adjusted -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <!-- A Web Form Control button for logging out user -->
                <asp:Button ID="logOutButton" runat="server" CssClass="btn btn-link logOutButton" CausesValidation="False" OnClick="logOutButton_Click" PostBackUrl="~/CommonPages/CommonPageHome.aspx" Text="LOG OUT" />
            </li>
        </ul>
    </div>
</nav>


