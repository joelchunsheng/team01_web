﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="ViewingRequestDetails.aspx.cs" Inherits="e_Portfolio_System.ParentPackage.ViewingRequestDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 428px;
        }
        .auto-style7 {
            width: 192px;
        }
        .auto-style8 {
            color: #FF3300;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <table class="w-100">
        <tr>
            <td class="auto-style1">CHILD&#39;S PARTICULARS</td>
            <td>PARENT/GUARDIAN&#39;S PARTICULARS</td>
        </tr>
        <tr>
            <td class="auto-style1">NAME:<br />
                <asp:TextBox ID="childName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="cfChildName" runat="server" ControlToValidate="childName" CssClass="auto-style8" Display="Dynamic" ErrorMessage="*Required"></asp:RequiredFieldValidator>
            </td>
            <td>NAME:<br />
                <asp:TextBox ID="parentName" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfParent" runat="server" ControlToValidate="parentName" CssClass="auto-style8" ErrorMessage="*Required"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="auto-style7"><br />
                <br />
            </td>
            <td><br />
            </td>
        </tr>
        <tr>
            <td class="auto-style1"><br />
            </td>
            <td><br />
            </td>
        </tr>
        <tr>
            <td class="auto-style1">&nbsp;</td>
            <td class="text-center">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" style="height: 33px" />
            </td>
        </tr>
    </table>
</asp:Content>
