﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.ModelBinding;

namespace e_Portfolio_System.ParentPackage
{
    public partial class StudentPortfolioList : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {


                displayPortfolioList();


            }

        }



        private void displayPortfolioList()
        {

            int currentParentId = (int)Session["ParentID"];

            //Read connection string 
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //Instantiate a SqlConnection object with the connection string read
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT Student.StudentID, Name, Course, ViewingRequest.Status, ParentID From Student " +
                "FULL JOIN ViewingRequest on Student.Name = ViewingRequest.StudentName " +
                "WHERE (ParentID != @currentParentId AND ViewingRequest.Status = 'R') " +
                "OR (ParentID != @currentParentId AND ViewingRequest.Status = 'P') OR (ViewingRequest.Status is null) " +
                "ORDER BY " +
                //"case ViewingRequest.Status " +
                //"when 'A' then 1 " +
                //"when 'P' then 2 " +
                //"when '3' then 3 " +
                //"else 4 end," +
                " student.StudentID ASC "
                , conn);


            // 2. define parameters used in command object
            SqlParameter paramID = new SqlParameter();
            paramID.ParameterName = "@currentParentId";
            paramID.Value = currentParentId;

            cmd.Parameters.Add(paramID);

            //Instantiate a DataAdapter object and pass the SqlCommand object created as parameter
            SqlDataAdapter daStudent = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from the database
            DataSet result = new DataSet();

            //Connection must be opened before any operations are made
            conn.Open();

            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet
            //DataSet "result" will store the result of SELECT operation
            daStudent.Fill(result, "StudentDetails");

            //Connection should always be closed, whether error occurs or not
            conn.Close();

            //Specify GridView to get data from table "StaffDetails" in DataSet "result"
            gvStudent.DataSource = result.Tables["StudentDetails"];

            //Display list of data in GridView
            gvStudent.DataBind();

        }

        protected void gvStudent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //Set page index to the page clicked by user
            gvStudent.PageIndex = e.NewPageIndex;
            // Display records on the new page
            displayPortfolioList();
        }

        //public IQueryable<Student> gvStudent_GetData([Control] Course? )
        //{
        //    Student showCourse = new Student();
        //    var query = showCourse.Students.Include(s => s.Enrollments.Select(e => e.Course));

        //    if (displayYear != null)
        //    {
        //        query = query.Where(s => s.Year == displayYear);
        //    }

        //    return query;
        //}


    }
}