﻿<%@ Page Title="" Language="C#" MasterPageFile="~/ParentPackage/ParentTemplate.Master" AutoEventWireup="true" CodeBehind="ConversationHistoryWithMentor.aspx.cs" Inherits="e_Portfolio_System.ParentPackage.ConversationHistoryWithMentor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <style type="text/css">
        .auto-style1 {
            color: #FF0000;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h2><Strong>Conversation History</Strong></h2>
    Your conversation, once it has begun, will be shown below. Send a message now!<br />
    <asp:GridView ID="gvMessage" runat="server" CellPadding="3" GridLines="None" Width="1100px" BackColor="White" BorderColor="White" BorderStyle="Ridge" BorderWidth="2px" CellSpacing="1">
        <FooterStyle BackColor="#C6C3C6" ForeColor="Black" />
        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#E7E7FF" />
        <PagerStyle BackColor="#C6C3C6" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#DEDFDE" ForeColor="Black" />
        <SelectedRowStyle BackColor="#9471DE" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F1F1F1" />
        <SortedAscendingHeaderStyle BackColor="#594B9C" />
        <SortedDescendingCellStyle BackColor="#CAC9C9" />
        <SortedDescendingHeaderStyle BackColor="#33276A" />
    </asp:GridView>
    <textarea id="taReply" cols="20" name="S1" rows="2" runat="server"></textarea>
    <asp:Button ID="btnReply" runat="server" Text="Reply" OnClick="btnReply_Click" style="height: 33px" />
    <asp:RequiredFieldValidator ID="rfvReply" runat="server" ControlToValidate="taReply" CssClass="auto-style1" Display="Dynamic" ErrorMessage="*Required"></asp:RequiredFieldValidator>
</asp:Content>
