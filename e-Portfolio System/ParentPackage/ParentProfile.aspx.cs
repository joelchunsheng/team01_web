﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.ParentPackage
{
    public partial class ParentProfile : System.Web.UI.Page
    {


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                if (Session["LoginID"] != null)
                {
                    lblName.Text = (string)Session["parentname"];
                    lblParentEmail.Text = (string)Session["emailaddr"];

                    displayChildPortfolioList();

                }



            }

        }

        private void displayChildPortfolioList()
        {

            int currentParentId = (int)Session["ParentID"];

            //Read connection string 
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();

            //Instantiate a SqlConnection object with the connection string read
            SqlConnection conn = new SqlConnection(strConn);

            //Instantiate a SqlCommand object, supply it with the SQL statement
            //SELECT that operates against the database, and the connection object
            //used for connecting to the database
            SqlCommand cmd = new SqlCommand("SELECT Student.StudentID, Name, Course, ViewingRequest.Status AS 'ViewingStatus', ParentID From Student " +
                "FULL JOIN ViewingRequest on Student.Name = ViewingRequest.StudentName " +
                "WHERE (ParentID = @currentParentId AND ViewingRequest.Status = 'A') " +
                "OR (ParentID = @currentParentId AND ViewingRequest.Status = 'P') " +
                "ORDER BY student.StudentID ASC ", conn);




            // 2. define parameters used in command object
            cmd.Parameters.AddWithValue("@currentParentId", currentParentId);


            //Instantiate a DataAdapter object and pass the SqlCommand object created as parameter
            SqlDataAdapter daChild = new SqlDataAdapter(cmd);

            //Create a Dataset object to contain the records retrieved from the database
            DataSet result = new DataSet();



            //Connection must be opened before any operations are made
            conn.Open();

            //Use DataAdapter to fetch data to a table "StaffDetails" in DataSet
            //DataSet "result" will store the result of SELECT operation
            daChild.Fill(result, "StudentDetails");

            //Connection should always be closed, whether error occurs or not
            conn.Close();

            //Specify GridView to get data from table "StaffDetails" in DataSet "result"
            gvChild.DataSource = result.Tables["StudentDetails"];

            //Display list of data in GridView
            gvChild.DataBind();

        }

    }
}