﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace e_Portfolio_System.ParentPackage
{
    public partial class ConversationHistoryWithMentor : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            displayMessage();
        }
        protected void btnReply_Click(object sender, EventArgs e)
        {


            int FromIDs = Convert.ToInt32(Session["ParentID"]);
            string reply = taReply.InnerText;
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("INSERT INTO Reply (MessageID,ParentID,Text) VALUES (@messageID,@parentID,@text)", conn);
            cmd.Parameters.AddWithValue("@messageID", Request.QueryString["MessageID"]);

            cmd.Parameters.AddWithValue("@parentID", FromIDs);
            cmd.Parameters.AddWithValue("@text", reply);
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            displayMessage();
        }

        public void displayMessage()
        {
            string strConn = ConfigurationManager.ConnectionStrings["Student_EPortfolio"].ToString();
            SqlConnection conn = new SqlConnection(strConn);
            SqlCommand cmd = new SqlCommand("SELECT * FROM Reply WHERE MessageID = @messageID", conn);
            cmd.Parameters.AddWithValue("@messageID", Request.QueryString["MessageID"]);
            SqlDataAdapter daReply = new SqlDataAdapter(cmd);
            DataSet result = new DataSet();
            conn.Open();
            daReply.Fill(result, "ReplyDetails");
            conn.Close();
            gvMessage.DataSource = result.Tables["ReplyDetails"];
            gvMessage.DataBind();

        }

       
    }
}